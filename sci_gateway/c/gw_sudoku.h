
// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

//
// gw_sudoku.h
//   Header for the SUDOKU gateway.
//
#ifndef __SCI_GW_SUDOKU_H__
#define __SCI_GW_SUDOKU_H__

	// Functions providing interfaces to Scilab functions.
	// These are the public functions of the gateway.
	//
	int sci_sudoku_solvefast(char* fname);


#endif /* __SCI_GW_SUDOKU_H__ */
