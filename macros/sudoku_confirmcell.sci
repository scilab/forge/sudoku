// Copyright (C) 2010 - 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [ X , C , L ] = sudoku_confirmcell ( varargin )
  //   Confirm the value of a cell.
  //
  // Calling Sequence
  //   [ X , C , L ] = sudoku_confirmcell ( X , C , L , i , j , v )
  //   [ X , C , L ] = sudoku_confirmcell ( X , C , L , i , j , v , verbose )
  //
  // Parameters
  // X: a 9-by-9 matrix, with 0 for unknown entries
  // C: a 9 x 9 cell of candidates
  // L: the 9 x 9 matrix of number candidates
  // i: the row of the value
  // j: the column of the value
  // v: the value to remove
  //
  // Description
  //   Confirms v as a value for X(i,j) and remove this value 
  //   from candidates of the row, column and block.
  //
  // Examples
  // X = [
  // 0 2 0   6 3 0   0 4 0
  // 6 0 0   0 0 0   0 0 3
  // 3 0 4   0 0 0   5 6 0
  // ..
  // 0 0 0   8 0 6   0 0 0
  // 8 0 0   0 1 0   0 0 6
  // 0 6 0   7 0 5   0 0 0
  // ..
  // 0 8 7   0 0 0   6 0 4
  // 4 0 0   0 6 7   0 0 8
  // 0 3 6   0 4 8   0 2 0
  // ];
  // [C,L] = sudoku_candidates(X);
  // [ X , C , L ] = sudoku_confirmcell ( X , C , L , 7 , 5 , 5 );
  //
  // Authors
  // Michael Baudin, 2010-2011
  
  [lhs, rhs] = argn()
  apifun_checkrhs ( "sudoku_confirmcell" , rhs , 6:7 )
  apifun_checklhs ( "sudoku_confirmcell" , lhs , 3 )
  //
  X = varargin(1)
  C = varargin(2)
  L = varargin(3)
  i = varargin(4)
  j = varargin(5)
  v = varargin(6)
  verbose = apifun_argindefault ( varargin , 7 , %f )
  //
  // Check type
  apifun_checktype ( "sudoku_candidateremove" , X , "X" , 1 , "constant" )
  apifun_checktype ( "sudoku_candidateremove" , C , "C" , 2 , "hypermat" )
  apifun_checktype ( "sudoku_candidateremove" , L , "L" , 3 , "constant" )
  apifun_checktype ( "sudoku_candidateremove" , i , "i" , 4 , "constant" )
  apifun_checktype ( "sudoku_candidateremove" , j , "j" , 5 , "constant" )
  apifun_checktype ( "sudoku_candidateremove" , v , "v" , 6 , "constant" )
  apifun_checktype ( "sudoku_candidateremove" , verbose , "verbose" , 7 , "boolean" )
  //
  // Check size
  apifun_checkdims ( "sudoku_candidateremove" , X , "X" , 1 , [9 9] )
  apifun_checkdims ( "sudoku_candidateremove" , C , "C" , 2 , [9 9 9] )
  apifun_checkdims ( "sudoku_candidateremove" , L , "L" , 3 , [9 9] )
  apifun_checkdims ( "sudoku_candidateremove" , i , "i" , 4 , [1 1] )
  apifun_checkdims ( "sudoku_candidateremove" , j , "j" , 5 , [1 1] )
  apifun_checkdims ( "sudoku_candidateremove" , v , "v" , 6 , [1 1] )
  apifun_checkdims ( "sudoku_candidateremove" , verbose , "verbose" , 7 , [1 1] )
  //
  // Confirm v for (i,j)
  X(i,j) = v
  C(i,j,:) = %f
  L(i,j) = 0
  if ( verbose ) then
    mprintf("Confirmed (%d,%d) as %d (remains %d unknowns)\n",i,j,v,sum(X==0))
  end
  // Remove v from row i of C
  C(i,:,v) = %f
  for c = 1 : 9
    L(i,c) = size(find(C(i,c,:)),"*")
  end
  // Remove v from column j of C
  C(:,j,v) = %f
  for r = 1 : 9
    L(r,j) = size(find(C(r,j,:)),"*")
  end
  // Remove v from block (i,j) of C
  C(tri(i),tri(j),v) = %f
  for r = tri(i)
    for c = tri(j)
      L(r,c) = size(find(C(r,c,:)),"*")
    end
  end

endfunction

//
// tri --
//   tri(1) = [1 2 3], tri(2) = [1 2 3], tri(3) = [1 2 3]
//   tri(4) = [4 5 6], tri(5) = [4 5 6], tri(6) = [4 5 6]
//   tri(7) = [7 8 9], tri(8) = [7 8 9], tri(9) = [7 8 9]
function y = tri ( k )
  y = 3*ceil(k/3-1) + (1:3)
endfunction
