// Copyright (C) 2010-2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function X = sudoku_rootsolution (  )
  //   Returns the root solution.
  //
  // Calling Sequence
  //   X = sudoku_rootsolution (  )
  //
  // Parameters
  // X: a 9-by-9 matrix
  //
  // Description
  //   Returns a filled sudoku, based on ordered permutations.
  //
  // Examples
  // X = sudoku_rootsolution ()
  //
  // Authors
  // Michael Baudin, 2010-2011
  //
  // Bibliography
  //  "Metaheuristics can solve sudoku puzzles", Rhyd Lewis, J Heuristics (2007) 13: 387?401

  [lhs, rhs] = argn()
  apifun_checkrhs ( "sudoku_rootsolution" , rhs , 0 )
  apifun_checklhs ( "sudoku_rootsolution" , lhs , 1 )
  //
  X = zeros(9,9)
  z = 0
  for i = 1 : 3
  for j = 1 : 3
    for k = 1 : 9
      X(3*(i-1)+j,k) = pmodulo(z,9) + 1
      z = z + 1
    end
    z = z + 3
  end
  z = z + 1
  end
endfunction

