// Copyright (C) 2010-2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function V = sudoku_visiblefrom ( a )
  //   Compute the cells visible from cell a.
  //
  // Calling Sequence
  //   V = sudoku_visiblefrom ( a )
  //
  // Parameters
  // a: a 1x2 matrix, where a(1) is a row index and a(2) is a column index
  // V: a 9x9 boolean matrix. V(i,j) is true if the cell (i,j) is visible from cell a.
  //
  // Description
  //   Returns the matrix of cells which are visible from both the cell 
  //   a. The cell p is visible from the cell a if it is in the same 
  //   row, column or box.
  //
  // Examples
  // 
  // V = sudoku_visiblefrom ( [1 2] ); sudoku_print(V)
  // V = sudoku_visiblefrom ( [2 3] ); sudoku_print(V)
  // V = sudoku_visiblefrom ( [3 4] ); sudoku_print(V)
  //
  // Authors
  //   Michael Baudin, 2010-2011
  //
  
  [lhs, rhs] = argn()
  apifun_checkrhs ( "sudoku_visiblefrom" , rhs , 1 )
  apifun_checklhs ( "sudoku_visiblefrom" , lhs , 1 )
  //
  // Check type
  apifun_checktype ( "sudoku_visiblefrom" , a , "a" , 1 , "constant" )
  //
  // Check size
  apifun_checkvector ( "sudoku_visiblefrom" , a , "a" , 1 , 2 )
  //
  ra = a(1)
  ca = a(2)
  V = ( zeros(9,9) == 1 )
  V(ra,:) = %t
  V(:,ca) = %t
  V(tri(ra),tri(ca)) = %t
endfunction
//
// tri --
//   tri(1) = [1 2 3], tri(2) = [1 2 3], tri(3) = [1 2 3]
//   tri(4) = [4 5 6], tri(5) = [4 5 6], tri(6) = [4 5 6]
//   tri(7) = [7 8 9], tri(8) = [7 8 9], tri(9) = [7 8 9]
function y = tri ( k )
  y = 3*ceil(k/3-1) + (1:3)
endfunction

