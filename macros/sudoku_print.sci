// Copyright (C) 2010 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function sudoku_print ( X )  
  //   Print a sudoku.
  //
  // Calling Sequence
  //   sudoku_print ( X )  
  //
  // Parameters
  // X: a 9-by-9 matrix of doubles, with 0 for unknown entries. Or a 9-by-9-by-9 hypermatrix of doubles, the candidates.
  //
  // Description
  //   Print a sudoku X, regrouping rows.
  //   Formats the printing to make the subblocks 
  //   clearer. Make so that the output can be directly 
  //   used for matrix input.
  //
  //   This function can also print candidates.
  //
  // Examples
  // X = [
  // 0 0 0 0 0 0 0 0 0
  // 0 0 0 0 0 0 5 3 0
  // 9 0 0 0 3 7 0 0 0
  // 0 0 2 3 0 1 0 9 6
  // 0 4 7 6 9 0 1 8 0
  // 0 0 6 7 8 5 0 0 0
  // 0 5 0 0 2 0 9 0 3
  // 0 3 0 9 0 0 6 0 8
  // 8 0 0 0 1 0 4 7 0
  // ];
  // sudoku_print(X);
  // [C,L] = sudoku_candidates(X);
  // sudoku_print(C);
  //
  // Authors
  //   Michael Baudin, 2010
  
  [lhs, rhs] = argn()
  apifun_checkrhs ( "sudoku_print" , rhs , 1 )
  apifun_checklhs ( "sudoku_print" , lhs , 1 )
  //
  // Check type
  apifun_checktype ( "sudoku_print" , X , "X" , 1 , ["constant" "hypermat"])
  //
  // Check size
  if ( or(type(X) == [1 4]) ) then
    apifun_checkdims ( "sudoku_print" , X , "X" , 1 , [9 9] )
  else
    apifun_checkdims ( "sudoku_print" , X , "X" , 1 , [9 9 9] )
  end
  //
  if ( type(X) == 1 ) then
    // Real matrix
    for i = 1 : 9
      for j = 1 : 9
        mprintf("%d ",X(i,j))
        if ( j == 3 | j == 6 ) then
          mprintf("  ")
        end
      end
      mprintf("\n")
      if ( i == 3 | i == 6 ) then
        mprintf("..\n")
      end
    end
    
  elseif ( type(X) == 4 ) then
    // Boolean matrix
    for i = 1 : 9
      for j = 1 : 9
        mprintf("%s ",string(X(i,j)))
        if ( j == 3 | j == 6 ) then
          mprintf("  ")
        end
      end
      mprintf("\n")
      if ( i == 3 | i == 6 ) then
        mprintf("..\n")
      end
    end
    
  else
    // Hypermatrix : a matrix of candidates
    //
    // Compute the maximum number of candidates per cell
    cmax = 0
    for i = 1 : 9
      for j = 1 : 9
        if ( size(find(X(i,j,:)),"*") > cmax ) then
          cmax = size(find(X(i,j,:)),"*")
        end
      end
    end
    // The required number of characters is 2*cmax+1:
    // One digit + One blank + ... + One digit + One blank + One extra blank
    cmax = 2*cmax + 1
    //
    // Compute a matrix of strings with the candidates.
    S = []
    for i = 1 : 9
      for j = 1 : 9
        nz = find(X(i,j,:))
        if ( nz == [] ) then
          S(i,j) = ""
        else
          snz = size(nz,"*")
          cs = matrix(nz,snz,1)
          S(i,j) = msprintf("{%s}",strcat(string(cs)," "))
        end
      end
    end
    //
    // Pad strings with blanks 
    for i = 1 : 9
      for j = 1 : 9
        lS = length(S(i,j))
        if ( lS < cmax ) then
          S(i,j) = S(i,j) + blanks(cmax - lS)
        end
      end
    end
    //
    // Compute a matrix of rows suitable for human reading
    k = 0
    for i = 1 : 9
      k = k + 1
      str(k) = ""
      for j = 1 : 9
        str(k) = str(k) + S(i,j)
        if ( modulo(j,3) == 0 & j < 9 ) then
          str(k) = str(k) + "|"
        end
      end
      if ( modulo(i,3) == 0 & i < 9 ) then
        k = k + 1
        str(k) = ""
        for km = 1 : cmax*9 + 3
          str(k) = str(k) + "-"
        end
      end
    end
    //
    // Print the rows
    for r = 1 : size(str,"r")
      mprintf("%s\n",str(r))
    end

  end
endfunction

