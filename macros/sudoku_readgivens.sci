// Copyright (C) 2010 - 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function X = sudoku_readgivens ( str )
  //   Read a sudoku from a string of 81 givens.
  //
  // Calling Sequence
  //   X = sudoku_readgivens ( str )
  //
  // Parameters
  // str: a string of 81 letters, with dots "." for unknowns. The entries come row by row.
  // X: a 9-by-9 matrix, with 0 for unknown entries
  //
  // Description
  //   Read a sudoku in a .sdk and return the matrix X associated,
  //   where 0 represent an unknown cell.
  //
  // Examples
  //    X = sudoku_readgivens ("6....23..1256.......47...2.73....84...........46....15.5...81.......3472..72....8");
  //    sudoku_print(X)
  //    // sudoku_solve(X)
  //
  //    X = sudoku_readgivens ("4...3.......6..8..........1....5..9..8....6...7.2........1.27..5.3....4.9........");
  //    sudoku_print(X)
  //    // sudoku_solve(X)
  //
  // See also
  //   sudoku_readsdk
  //   sudoku_readsdm
  //   sudoku_readsdmnb
  //   sudoku_readgivens
  //   sudoku_readss
  //   sudoku_writesdk
  //   sudoku_writegivens
  //   sudoku_writess
  //
  // Authors
  //   Michael Baudin, 2010-2011
  //
  // Bibliography
  //   http://hodoku.sourceforge.net/en/docs_play.php#filters

  [lhs, rhs] = argn()
  apifun_checkrhs ( "sudoku_readgivens" , rhs , 1 )
  apifun_checklhs ( "sudoku_readgivens" , lhs , 1 )
  //
  // Check type
  apifun_checktype ( "sudoku_readgivens" , str , "str" , 1 , "string" )
  //
  // Check size
  apifun_checkscalar ( "sudoku_print" , str , "str" , 1 )
  //
  if ( length(str)<>81 ) then
    errmsg = msprintf(gettext("%s: The given string has %d instead of 81 characters."), "sudoku_readgivens",length(str));
    error(errmsg)
  end

  X = zeros(9,9)

  k = 0
  for i = 1 : 9
    for j = 1 : 9
      k = k + 1 
      ch = part(str,k)
      if ( ch <> "." ) then
        X(i,j) = msscanf(ch,"%d")
      end
    end
  end
endfunction


