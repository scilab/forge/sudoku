// Copyright (C) 2010 - 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [X,C,L] = sudoku_findhiddensubset ( varargin )
  //   Find naked subsets of given length.
  //
  // Calling Sequence
  //   X = sudoku_findhiddensubset ( X , C , L , k )
  //   X = sudoku_findhiddensubset ( X , C , L , k , stopatfirst )
  //   X = sudoku_findhiddensubset ( X , C , L , k , stopatfirst , verbose )
  //   [X,C] = sudoku_findhiddensubset ( ... )
  //   [X,C,L] = sudoku_findhiddensubset ( ... )
  //
  // Parameters
  // X: a 9-by-9 matrix of doubles, with 0 for unknown entries
  // C: a 9-by-9-by-9 hypermatrix of doubles, the candidates
  // L: a 9-by-9 matrix of doubles, the number candidates
  // k: the length of the naked subset
  // stopatfirst: if %t, then stop when one or more candidates have been removed. (default = %t)
  // verbose: a boolean. Set to %t to display the state of the matrix. (default = %f)
  //
  // Description
  //   Search for hidden subsets of length k.
  //   For example, k=1 finds hidden singles, k=2 finds hidden pairs,
  //   k=3 finds hidden triples and k=4 finds hidden quads.
  //
  //   The algorithm computes a merged list of all candidates in the row, column
  //   or block. This merged list is simplified and only unique candidates are kept.
  //   The list is used to generate all combinations of k elements.
  //   Each combination S is searched in the row, column or block.
  //   The row, column or block contains a hidden subset of length k if exactly k  
  //   cells are so that any item in S is contained in the candidates of each cell.
  //   If a hidden subset of length k is found, the candidates which are not in S
  //   are removed from the k cells.
  // 
  //   This generic algorithm can be specialized to find hidden pairs, hidden triples,
  //   hidden quads, hidden sextets, hidden septets and hidden octets.
  //   See the note in findnakedsubset.
  //   
  //   See the note on interrupting the algorithm.
  //
  // Examples
  // X = [
  // 4 0 0   3 9 0   0 0 2
  // 2 6 0   0 5 8   3 9 0
  // 5 9 3   6 0 0   1 8 0
  // ..
  // 1 0 0   8 6 0   0 0 9
  // 6 0 5   9 0 0   2 0 0 
  // 0 3 9   2 4 5   0 1 6
  // ..
  // 0 5 6   0 0 9   0 2 0
  // 0 1 4   7 0 0   9 0 5
  // 9 0 0   5 3 0   0 0 0
  // ];
  // [C,L] = sudoku_candidates(X);
  //   // Find triple {1,7,8} in row 1 at (1,2), (1,3), (1,6).
  // sudoku_findhiddensubset ( X , C , L , %t ); 
  //
  // Authors
  //   Michael Baudin, 2010-2011
  //
  
  [lhs, rhs] = argn()
  apifun_checkrhs ( "sudoku_findhiddensubset" , rhs , 4:6 )
  apifun_checklhs ( "sudoku_findhiddensubset" , lhs , 1:3 )
  //
  X = varargin(1)
  C = varargin(2)
  L = varargin(3)
  k = varargin(4)
  stopatfirst = apifun_argindefault ( varargin , 5 , %t )
  verbose = apifun_argindefault ( varargin , 6 , %f )
  //
  // Check type
  apifun_checktype ( "sudoku_findhiddensubset" , X , "X" , 1 , "constant" )
  apifun_checktype ( "sudoku_findhiddensubset" , C , "C" , 2 , "hypermat" )
  apifun_checktype ( "sudoku_findhiddensubset" , L , "L" , 3 , "constant" )
  apifun_checktype ( "sudoku_findhiddensubset" , k , "k" , 4 , "constant" )
  apifun_checktype ( "sudoku_findhiddensubset" , stopatfirst , "stopatfirst" , 5 , "boolean" )
  apifun_checktype ( "sudoku_findhiddensubset" , verbose , "verbose" , 6 , "boolean" )
  //
  // Check size
  apifun_checkdims ( "sudoku_findhiddensubset" , X , "X" , 1 , [9 9] )
  apifun_checkdims ( "sudoku_findhiddensubset" , C , "C" , 2 , [9 9 9] )
  apifun_checkdims ( "sudoku_findhiddensubset" , L , "L" , 3 , [9 9] )
  apifun_checkscalar ( "sudoku_findhiddensubset" , k , "k" , 4 )
  apifun_checkscalar ( "sudoku_findhiddensubset" , stopatfirst , "stopatfirst" , 5 )
  apifun_checkscalar ( "sudoku_findhiddensubset" , verbose , "verbose" , 6 )
  //
  before = sum(L)
  // Search for subsets in rows
  for i = 1 : 9
    // See if there can be a subset of length k in this row
    n = sum(X(i,:)==0)
    kmax = ceil(n/2)
    if ( k > kmax ) then
      // There is no subset of length k, try next.
      continue
    end
    // Search for subsets in row i.
    // Assemble all candidates of the row.
    carray  = sudoku_candidatesmerge ( X , C , L , 1 , i , [] , [1 9] )
    if ( carray == [] ) then
      // There are no possible subset in this row, go on to the next
      continue
    end
    if ( size(carray,"*") <= k ) then
      // There are less than k candidates in this row:
      // there cannot exist a subset here, try the next row.
      continue
    end
    // Get all possible combinations of k elements among 
    // these candidates
    cmap = combine ( carray , k )
    nk = size(cmap,"r")
    // Search for each combinations in the row i
    for m = 1 : nk
      S = cmap(m,:)
      icols = []
      for c = 1 : 9
        if ( X(i,c) == 0 ) then
          if ( isAnyContained ( S , find(C(i,c,:)) ) ) then
            // This might be a subset: register it
            icols($+1) = c
            if ( size(icols,"*") > k ) then
              // There are already more than k cells containing the subset.
              break
            end
          end
        end
      end
      if ( size(icols,"*") <> k ) then
        // This subset is not represented in the row:
        // try next subset.
        continue
      end
      if ( k == 1 ) then
        // This is a hidden single.
        c = icols(1)
        if ( verbose ) then
          mprintf("Found hidden single in row at (%d,%d) as %d\n",i,c,S)
        end
        [ X , C , L ] = sudoku_confirmcell ( X , C , L , i , c , S , verbose )
        // Try next subset.
        continue
      end
      // This is a hidden subset: remove the other candidates 
      // from these k cells.
      firstverb = %t
      for ik = 1 : k
        // Simplify the cell #ik
        c = icols(ik)
        // Remove each candidate which is different from the subset S.
        for v = find(C(i,c,:))
          if ( and(S<>v) ) then
            [ C , L ] = sudoku_candidateremove ( C , L , 4 , i , c , v )
            if ( verbose ) then
              if ( firstverb ) then
                mprintf("Found hidden subset {%s} in row %d at columns %s\n" , ..
                  strcat(string(S),","),i,strcat(string(icols)," "))
                firstverb = %f
              end
              mprintf("Removed other candidates %d in hidden subset at (%d,%d) (remaining %d candidates)\n",v,i,c,sum(L))
            end
          end
        end
      end
      if ( stopatfirst & sum(L) <> before ) then
        return
      end
    end
  end
  
  // Search for subsets in columns
  for j = 1 : 9
    // See if there can be a subset of length k in this column
    n = sum(X(:,j)==0)
    kmax = ceil(n/2)
    if ( k > kmax ) then
      // There is no subset of length k, try next.
      continue
    end
    // Search for subsets in column j.
    // Assemble all candidates of the column.
    carray  = sudoku_candidatesmerge ( X , C , L , 2 , [] , j , [1 9] )
    if ( carray == [] ) then
      // There are no possible subset in this column, go on to the next
      continue
    end
    if ( size(carray,"*") <= k ) then
      // There are less than k candidates in this column:
      // there cannot exist a subset here, try the next column.
      continue
    end
    // Get all possible combinations of k elements among 
    // these candidates.
    cmap = combine ( carray , k )
    nk = size(cmap,"r")
    // Search for each combinations in the column i
    for m = 1 : nk
      S = cmap(m,:)
      irows = []
      for r = 1 : 9
        if ( X(r,j) == 0 ) then
          if ( isAnyContained ( S , find(C(r,j,:)) ) ) then
            // This might be a subset: register it
            irows($+1) = r
            if ( size(irows,"*") > k ) then
              // There are already more than k cells containing the subset.
              break
            end
          end
        end
      end
      if ( size(irows,"*") <> k ) then
        // This subset is not represented in the column:
        // try next subset.
        continue
      end
      if ( k == 1 ) then
        // This is a hidden single.
        r = irows(1)
        if ( verbose ) then
          mprintf("Found hidden single in column at (%d,%d) as %d\n",r,j,S)
        end
        [ X , C , L ] = sudoku_confirmcell ( X , C , L , r , j , S , verbose )
        // Try next subset.
        continue
      end
      // This is a hidden subset: remove the other candidates 
      // from these k cells.
      firstverb = %t
      for ik = 1 : k
        // Simplify the cell #ik
        r = irows(ik)
        // Remove each candidate which is different from the subset.
        for v = find(C(r,j,:))
          if ( and(S<>v) ) then
            [ C , L ] = sudoku_candidateremove ( C , L , 4 , r , j , v )
            if ( verbose ) then
              if ( firstverb ) then
                mprintf("Found hidden subset {%s} in column %d at rows %s\n" , ..
                  strcat(string(S),","),j,strcat(string(irows)," "))
                firstverb = %f
              end
              mprintf("Removed other candidates %d in hidden pair at (%d,%d) (remaining %d candidates)\n",v,r,j,sum(L))
            end
          end
        end
      end
      if ( stopatfirst & sum(L) <> before ) then
        return
      end
    end
  end
  // Search for subsets in blocks
  for i = [1 4 7]
    for j = [1 4 7]
      // See if there can be a subset of length k in this block
      n = sum(X(tri(i),tri(j))==0)
      kmax = ceil(n/2)
      if ( k > kmax ) then
        // There is no subset of length k, try next.
        continue
      end
      // Search for subsets in block (i,j)
      // Assemble all candidates of the block
      carray  = sudoku_candidatesmerge ( X , C , L , 3 , i , j , [1 9] )
      if ( carray == [] ) then
        // There are no possible subset in this block, go on to the next
        continue
      end
      if ( size(carray,"*") <= k ) then
        // There are less than k candidates in this block:
        // there cannot exist a subset here, try the next block.
        continue
      end
      // Get all possible combinations of 3 elements among 
      // these candidates
      cmap = combine ( carray , k )
      nk = size(cmap,"r")
      // Search for each combinations in the block (i,j)
      for m = 1 : nk
        S = cmap(m,:)
        irows = []
        icols = []
        for r = tri(i)
          for c = tri(j)
            if ( X(r,c) == 0 ) then
              if ( isAnyContained ( S , find(C(r,c,:)) ) ) then
                // This might be a subset: register it
                irows($+1) = r
                icols($+1) = c
                if ( size(irows,"*") > k ) then
                  // There are already more than k cells containing the subset.
                  break
                end
              end
            end
          end
          if ( size(irows,"*") > k ) then
            break
          end
        end
        if ( size(irows,"*") <> k ) then
          // This subset is not represented in the block
          // Try next subset
          continue
        end
        if ( k == 1 ) then
          // This is a hidden single.
          r = irows(1)
          c = icols(1)
          if ( verbose ) then
            mprintf("Found hidden single in block at (%d,%d) as %d\n",r,c,S)
          end
          [ X , C , L ] = sudoku_confirmcell ( X , C , L , r , c , S , verbose )
          // Try next subset.
          continue
        end
        // This is a hidden subset: remove the other candidates 
        // from these k cells.
        firstverb = %t
        for ik = 1 : k
          // Simplify the cell #ik
          r = irows(ik)
          c = icols(ik)
          // Remove each candidate which is different from the subset.
          for v = find(C(r,c,:))
            if ( and(S<>v) ) then
              [ C , L ] = sudoku_candidateremove ( C , L , 4 , r , c , v )
              if ( verbose ) then
                if ( firstverb ) then
                  mprintf("Found hidden pair {%s} in block (%d,%d) at rows %s and columns %s\n" , ..
                    strcat(string(S),","),i,j,strcat(string(irows)," "),strcat(string(icols)," "))
                  firstverb = %f
                end
                mprintf("Removed other candidates %d in hidden subset at (%d,%d) (remaining %d candidates)\n",v,r,c,sum(L))
              end
            end
          end
        end
        if ( stopatfirst & sum(L) <> before ) then
          return
        end
      end
    end
  end
  
endfunction
// Generates an error if the given variable is not of type hypermat
function apifun_typehypermat ( var , varname , ivar )
  if ( typeof ( var ) <> "hypermat" ) then
    errmsg = msprintf(gettext("%s: Expected hypermat variable for variable %s at input #%d, but got %s instead."),"apifun_typehypermat", varname , ivar , typeof(var) );
    error(errmsg);
  end
endfunction

// Generates an error if the given variable is not of type boolean
function apifun_typeboolean ( var , varname , ivar )
  if ( type ( var ) <> 4 ) then
    errmsg = msprintf(gettext("%s: Expected boolean but for variable %s at input #%d, got %s instead."),"apifun_typeboolean", varname , ivar , typeof(var) );
    error(errmsg);
  end
endfunction
// Generates an error if the given variable is not of type real
function apifun_typereal ( var , varname , ivar )
  if ( type ( var ) <> 1 ) then
    errmsg = msprintf(gettext("%s: Expected real variable for variable %s at input #%d, but got %s instead."),"apifun_typereal", varname , ivar , typeof(var) );
    error(errmsg);
  end
endfunction
// Generates an error if the given variable is not of type cell
function apifun_typecell ( var , varname , ivar )
  if ( typeof ( var ) <> "ce" ) then
    errmsg = msprintf(gettext("%s: Expected cell variable for variable %s at input #%d, but got %s instead."),"apifun_typecell", varname , ivar , typeof(var) );
    error(errmsg);
  end
endfunction


// Returns the (i,j) index given the global index s in a block
// Note: in Scilab, entries are numbered column bX column.
//  whatcellinblock ( 1 ) = (1,1)
//  whatcellinblock ( 2 ) = (2,1)
//  whatcellinblock ( 3 ) = (3,1)
//  whatcellinblock ( 4 ) = (1,2)
//  ...
//  whatcellinblock ( 9 ) = (9,1)
function [i,j] = whatcellinblock ( s )
  i = modulo(s-1,3) + 1
  j = (s-i)/3 + 1
endfunction

// combine --
// Returns all combinations of k values from the row vector x as a row-by-row array
// with (n,k) rows where (n,k) is the binomial coefficient and n is the 
// number of values in x. 
//
// References
// http://home.att.net/~srschmitt/script_combinations.html
// Kenneth H. Rosen, Discrete Mathematics and Its Applications, 2nd edition (NY: McGraw-Hill, 1991), pp. 284-286.
//
// Example
//   combine ( [17 32 48 53] , 3 ) :
//       17.    32.    48.  
//       17.    32.    53.  
//       17.    48.    53.  
//       32.    48.    53.  
//
function cmap = combine ( x , k )
  n = size(x,"*")
  if ( n < k ) then
    error ( msprintf ( "%s: The number of values in x is n = %d which is lower than k = %d\n", n,k))
  end
  c = combinations ( n , k )
  cmap = zeros(c,k)
  a = 1:k
  cmap(1,:) = x(a)
  for m = 2 : c
    i = k
    while ( a(i) == n - k + i )
      i = i - 1
    end
    a(i) = a(i) + 1
    for j = i+1 : k
      a(j) = a(i) + j - i
    end
    cmap(m,:) = x(a)
  end
endfunction
//
// whatcell --
// Returns the (i,j) index given the global index s
// Note: in Scilab, entries are numbered column by column.
//
// Example
//  whatcell ( 1 ) = (1,1)
//  whatcell ( 2 ) = (2,1)
//  whatcell ( 3 ) = (3,1)
//  ...
//  whatcell ( 9 ) = (9,1)
//  whatcell ( 10 ) = (1,2)
//  whatcell ( 12 ) = (2,2)
function [i,j] = whatcell ( s )
  i = modulo(s-1,9) + 1
  j = (s-i)/9 + 1
endfunction

// isAnyContained --
// Returns %t if any item in the row vector x is in the 
// row set S.
//
// Example 
//   isAnyContained ( [3 50 51] , 1:2:11 ) is true
//   isAnyContained ( [49 50 51] , 1:2:11 ) is false
function bool = isAnyContained ( x , S )
  bool = %f
  for v = x
    if ( find(S==v) <> [] ) then
      bool = %t
      break
    end
  end
endfunction


// combinations --
//   Returns the number of combinations of j objects chosen from n objects.
//   If the input where integers, returns also an integer.
function c = combinations ( n , j )
  c = exp(gammaln(n+1)-gammaln(j+1)-gammaln(n-j+1));
  if ( and(round(n)==n) & and(round(j)==j) ) then
    c = round ( c )
  end
endfunction

