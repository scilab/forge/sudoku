// Copyright (C) 2010-2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function str = sudoku_writess ( varargin )
  //   Returns the given sudoku as a Simple Sudoku format.
  //
  // Calling Sequence
  //   str = sudoku_writess ( X )
  //   str = sudoku_writess ( X , filename )
  //
  // Parameters
  // X: a 9-by-9 matrix, with 0 for unknown entries
  // filename, optional: a string representing a file
  // str: a 11-by-1 matrix strings, with dots "." for unknowns. The entries come row by row.
  //
  // Description
  //   Converts a sudoku in human readable format string, where "." represents unknowns.
  //   If the input argument filename is present, also writes the string
  //   into the file.
  //
  // Examples
  // X = [
  // 0 0 0   0 0 0   0 0 0
  // 0 0 0   0 0 0   5 3 0
  // 9 0 0   0 3 7   0 0 0
  // 0 0 2   3 0 1   0 9 6
  // 0 4 7   6 9 0   1 8 0
  // 0 0 6   7 8 5   0 0 0
  // 0 5 0   0 2 0   9 0 3
  // 0 3 0   9 0 0   6 0 8
  // 8 0 0   0 1 0   4 7 0
  // ]
  // str = sudoku_writess ( X )
  // // Write into a file:
  // filename = fullfile(TMPDIR,"foo.sdk");
  // sudoku_writess ( X , filename )
  // editor(filename)
  //
  // See also
  //   sudoku_readsdk
  //   sudoku_readsdm
  //   sudoku_readsdmnb
  //   sudoku_readgivens
  //   sudoku_readss
  //   sudoku_writesdk
  //   sudoku_writegivens
  //   sudoku_writess
  //
  // Authors
  //   Michael Baudin, 2010-2011
  //
  // Bibliography
  //   http://www.sudocue.net/fileformats.php

  [lhs, rhs] = argn()
  apifun_checkrhs ( "sudoku_writess" , rhs , 1:2 )
  apifun_checklhs ( "sudoku_writess" , lhs , 1 )
  //
  X = varargin(1)
  filename = apifun_argindefault ( varargin , 2 , "" )
  //
  // Check type
  apifun_checktype ( "sudoku_writess" , X , "X" , 1 , "constant" )
  apifun_checktype ( "sudoku_writess" , filename , "filename" , 2 , "string" )
  //
  // Check size
  apifun_checkdims ( "sudoku_writess" , X , "X" , 1 , [9 9] )
  apifun_checkscalar ( "sudoku_writess" , filename , "filename" , 2 )
  //
  k = 0
  for i = 1 : 9
    k = k + 1
    str(k) = ""
    for j = 1 : 9
      if ( X(i,j) == 0 ) then
        str(k) = str(k) + "."
      else
        str(k) = str(k) + msprintf("%d",X(i,j))
      end
      if ( modulo(j,3) == 0 ) then
        str(k) = str(k) + "|"
      end
    end
    if ( modulo(i,3) == 0 & i < 9 ) then
      k = k + 1
      str(k) = "-----------"
    end
  end
  
  // Writes the file
  if ( filename <> "" ) then
    len = size(str,"r")
    [fd,err] = mopen ( filename , "w" )
    for k = 1 : len
      mfprintf(fd,"%s\n",str(k))
    end
    mclose ( fd )
  end
endfunction
