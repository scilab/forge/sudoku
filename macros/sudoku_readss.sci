// Copyright (C) 2010-2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function X = sudoku_readss ( filename )
    //   Read a sudoku in a .ss file.
    //
    // Calling Sequence
    //   X = sudoku_readss ( filename )
    //
    // Parameters
    // X: a 9-by-9 matrix, with 0 for unknown entries
    // filename: a string representing a file
    //
    // Description
    //   Read a sudoku in a .ss and return the matrix X associated,
    //   where 0 represent an unknown cell.
    //
    // Examples
    //    path = sudoku_getpath();
    //    filename = fullfile(path,"tests","unit_tests","minimal.ss");
    //    X = sudoku_readss ( filename )
    // 
    // See also
    //   sudoku_readsdk
    //   sudoku_readsdm
    //   sudoku_readsdmnb
    //   sudoku_readgivens
    //   sudoku_readss
    //   sudoku_writesdk
    //   sudoku_writegivens
    //   sudoku_writess
    //
    // Authors
    //   Michael Baudin, 2010-2011
    //
    // Bibliography
    //   http://www.sadmansoftware.com/sudoku/faq19.htm

  [lhs, rhs] = argn()
  apifun_checkrhs ( "sudoku_readss" , rhs , 1 )
  apifun_checklhs ( "sudoku_readss" , lhs , 1 )
  //
  // Check type
  apifun_checktype ( "sudoku_readss" , filename , "filename" , 1 , "string" )
  //
  // Check size
  apifun_checkscalar ( "sudoku_readss" , filename , "filename" , 1 )
  //
    if ( fileinfo(filename)==[] ) then
        errmsg = msprintf(gettext("%s: The file %s does not exist."), "sudoku_readss", filename);
        error(errmsg)
    end
    i = 0
    X = zeros(9,9)
    buffer = read(filename,-1,1,"(A)")
    nblines = size(buffer,"r")
    for k = 1 : nblines
        row = buffer ( k )
        row = stripblanks(row)
        firstchar = part(row,1)
        if (row=="|---+---+---|") then
            // Skip this line
            continue
        end
        if (row=="*-----------*") then
            // Skip this line
            continue
        end
        if ( firstchar <> "-" ) then
            i = i + 1
            clen = length(row)
            j = 0
            for c = 1 : clen
                ch = part(row,c)
                if ( ch <> "|" ) then
                    j = j + 1
                    if ( ch <> "." ) then
                        X(i,j) = msscanf(ch,"%d")
                    end
                end
            end
        end
    end
endfunction


