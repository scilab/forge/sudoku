// Copyright (C) 2010-2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [X,iter,C,L,conflict] = sudoku_solvebylogic ( varargin )
  //   Solves Sudoku.
  //
  // Calling Sequence
  //   Y = sudoku_solvebylogic ( X )
  //   Y = sudoku_solvebylogic ( X , verbose )
  //   [Y,iter] = sudoku_solvebylogic ( ... )
  //   [Y,iter,C] = sudoku_solvebylogic ( ... )
  //   [Y,iter,C,L] = sudoku_solvebylogic ( ... )
  //   [Y,iter,C,L,conflict] = sudoku_solvebylogic ( ... )
  //
  // Parameters
  // X: a 9-by-9 matrix, with 0 for unknown entries
  // verbose: a boolean. Set to %t to display the state of the matrix. (default = %f)
  // Y: a 9-by-9 matrix. If Y contains zeros, they stand for unsolved entries. The test and(Y(:) > 0) allows to see if the puzzle is solved.
  // iter: the number of steps required by each algorithm
  // iter(1): number naked singles steps
  // iter(2): number hidden singles steps
  // iter(3): number locked candidates steps
  // iter(4): number naked pairs steps
  // iter(5): number hidden pair steps
  // iter(6): number naked triples steps
  // iter(7): number hidden triples steps
  // iter(8): number naked quad steps 
  // iter(9): number hidden quad steps 
  // iter(10): number X-Wings steps 
  // iter(11): number Bi-Coloring steps 
  // iter(12): number X-Cycle steps 
  // C: a cell array of candidate vectors for each cell.
  // L: the number of candidates for each cell
  // conflict: %t if a conflict has been detect (i.e. it is guaranteed that the sudoku has no solution), %f if not.
  //
  // Description
  //   Using logic-based algorithm only.
  //   This is a combination of various algorithms :
  //   1 naked singles,
  //   2 hidden singles,
  //   3 locked candidates,
  //   4 naked pairs,
  //   5 hidden pairs.
  //   6 naked triples,
  //   7 hidden triples,
  //   8 naked quads,
  //   9 hidden quads,
  //   10 X-Wings.
  //   11 2-colors
  //   12 X-Cycles
  //
  //   The algorithm uses naked singles as much as it can.
  //   If no improvement can be done with naked singles,
  //   we search for hidden singles.
  //   If hidden singles were able to improve the sudoku,
  //   we switch back to naked singles.
  //   If no hidden single was able to improve the sudoku, 
  //   we search for locked candidates.
  //   This process is repeated for each searching algorithm,
  //   until either the sudoku is solved and we quit, or the sudoku is unsolved,
  //   and we quit.
  //
  //   The measure of the progress of each step uses both the number of unknowns
  //   and the number of candidates. If the number of unknowns or the number 
  //   of candidates has decreased, there was an improvement.
  //   The fact that the candidates are taked into account for the progress
  //   measure allows to solve the most difficult sudokus, where the best that 
  //   we can expect is that each algorithm only allows to remove some candidates
  //   until we finally can identify either a naked single or a hidden single,
  //   and thus, confirm a cell.
  //
  //   The iter array allows to measure the difficulty of the puzzle.
  //   Largest non-zero entries in the array indicate a larger number of 
  //   different strategy required. This can be computed with nstrats = sum(iter>0).
  //   The number of rounds, i.e. the number of global iterations through 
  //   the strategies can be deduced with nrounds = sum(iter).
  //   A larger number of rounds indicate a larger number of interactions between 
  //   different strategies.
  //
  //   When an unknown cell with no candidates is detected, there is a conflict
  //   and we are sure that the sudoku has no solution.
  //   In this case, the algorithms stops and returns.
  //   This may save a lot of iterations which could possibly set 
  //   digits in the sudoku, and would finally fail anyway.
  //
  //   Note on hidden and naked subsets.
  // 
  //   Consider a row, column or block with n unfixed entries. Assume that there are 
  //   p cells with a naked subset with candidates (a1,a2,...,ap). 
  //   We can prove that there is an
  //   hidden subset of length n-p. Indeed, consider the situation 
  //   of the n-p cells, after the resolution of the naked subset:
  //   the candidates (a1,a2,...,ap) have been eliminated. Therefore, before
  //   the resolution, there was n-p cells containing n-p candidates plus 
  //   the additionnal candidates (a1,a2,...,ap). Therefore, this was an 
  //   hidden subset with n-p cells.
  //   For example, if there is a row with 5 unfixed cells, and if there is 
  //   a hidden quad (4 candidates), there is also a naked single.
  //
  //   The corollary of the previous result is that, if we have procedures
  //   to find naked subsets of length 1, 2, 3, 4 and hidden subsets of length
  //   1, 2, 3, 4, it is useless to search for naked or hidden subsets of length
  //   5, 6, 7, 8. Let us prove this.
  //   Consider the worst situation of a row, column or block where there
  //   are 9 unfixed cells. There are at most 9 candidates in this area.
  //   If there is an hidden quintet (5 candidates), there is also a naked quad. If there is 
  //   an hidden sextet (6 candidates), there is also an naked triple. And so on.
  //
  //   TODO : use Alternating Inference Chain strategies.
  //
  // Examples
  // X = [
  // 0 0 0   0 0 0   0 0 0
  // 0 0 0   0 0 0   5 3 0
  // 9 0 0   0 3 7   0 0 0
  // 0 0 2   3 0 1   0 9 6
  // 0 4 7   6 9 0   1 8 0
  // 0 0 6   7 8 5   0 0 0
  // 0 5 0   0 2 0   9 0 3
  // 0 3 0   9 0 0   6 0 8
  // 8 0 0   0 1 0   4 7 0
  // ]
  // sudoku_solvebylogic(X)
  // sudoku_solvebylogic(X,%t)
  //
  // X = [
  // 0 0 0   0 0 0   0 0 0
  // 9 0 0   0 0 4   0 3 6
  // 0 0 0   0 7 2   0 0 9
  // 3 0 0   0 0 0   0 0 5
  // 0 1 4   0 0 0   8 0 0
  // 0 2 5   0 1 0   0 0 0
  // 0 0 6   1 0 0   0 0 3
  // 0 0 0   0 0 6   0 4 0
  // 0 0 2   4 8 9   0 0 0
  // ]
  // sudoku_solvebylogic(X)
  //
  // X = [
  // 1 2 0    0 3 0    0 4 0  
  // 6 0 0    0 0 0    0 0 3  
  // 3 0 4    0 0 0    5 0 0  
  // 2 0 0    8 0 6    0 0 0  
  // 8 0 0    0 1 0    0 0 6  
  // 0 0 0    7 0 5    0 0 0  
  // 0 0 7    0 0 0    6 0 0  
  // 4 0 0    0 0 0    0 0 8  
  // 0 3 0    0 4 0    0 2 0  
  // ]
  // sudoku_solvebylogic(X)
  //
  // X = [
  // 0 2 0   0 3 0   0 4 0
  // 6 0 0   0 0 0   0 0 3
  // 0 0 4   0 0 0   5 0 0
  // 0 0 0   8 0 6   0 0 0
  // 8 0 0   0 1 0   0 0 6
  // 0 0 0   7 0 5   0 0 0
  // 0 0 7   0 0 0   6 0 0
  // 4 0 0   0 0 0   0 0 8
  // 0 3 0   0 4 0   0 2 0
  // ]
  // sudoku_solvebylogic(X)
  //
  // Authors
  // Michael Baudin, 2010-2011
  //
  // Bibliography
  //   Programming Sudoku, Wei-Meng Lee, 2006
  //   Royle Gordon, http://units.maths.uwa.edu.au/~gordon/sudokumin.php

  [lhs, rhs] = argn()
  apifun_checkrhs ( "sudoku_solvebylogic" , rhs , 1:2 )
  apifun_checklhs ( "sudoku_solvebylogic" , lhs , 1:5 )
  //
  X = varargin(1)
  verbose = apifun_argindefault ( varargin , 2 , %f )
  //
  // Check type
  apifun_checktype ( "sudoku_solve" , X , "X" , 1 , "constant" )
  apifun_checktype ( "sudoku_solve" , verbose , "verbose" , 2 , "boolean" )
  //
  // Check size
  apifun_checkdims ( "sudoku_solve" , X , "X" , 1 , [9 9] )
  apifun_checkscalar ( "sudoku_solve" , verbose , "verbose" , 2 )
  //
  [C,L] = sudoku_candidates(X)
  //
  // Implement an state machine with a while and labels.
  NAKED_SINGLE = 1
  HIDDEN_SINGLE = 2
  LOCKED = 3
  NAKED_PAIR = 4
  HIDDEN_PAIR = 5
  NAKED_TRIPLE = 6
  HIDDEN_TRIPLE = 7
  NAKED_QUAD = 8
  HIDDEN_QUAD = 9
  XWING = 10
  BICOLOR = 11
  XCYCLE = 12
  FINISH = 13
  //
  // Create a map from strategy index to strategy name
  index2name(NAKED_SINGLE) = "Naked Singles"
  index2name(HIDDEN_SINGLE) = "Hidden Singles"
  index2name(LOCKED) = "Locked Candidates"
  index2name(NAKED_PAIR) = "Naked Pairs"
  index2name(HIDDEN_PAIR) = "Hidden Pairs"
  index2name(NAKED_TRIPLE) = "Naked Triples"
  index2name(HIDDEN_TRIPLE) = "Hidden Triples"
  index2name(NAKED_QUAD) = "Naked Quad"
  index2name(HIDDEN_QUAD) = "Hidden Quad"
  index2name(XWING) = "X Wing"
  index2name(BICOLOR) = "Bi-Coloring"
  index2name(XCYCLE) = "X-Cycle"
  //
  // Initialize the number of iterations 
  iter = zeros(FINISH-1,1)
  // If one state does not reduce the number of unknowns, 
  // we go to the next step, if any.
  // If one state was able to reduce the number of unknowns,
  // we got to the first step.
  // This system allows to avoid 5 nested loops
  // and also avoids to use gotos.
  state = NAKED_SINGLE
  conflict = %f
  while ( %t )
    if ( state == FINISH ) then
      break
    end
    //
    // Detect conflicts in the sudoku
    s = find ( X == 0 & L == 0 )
    if ( s <> [] ) then
      s = s(1)
      if ( verbose ) then
        [i,j] = whatcell ( s )
        mprintf ("Conflict in the sudoku at cell (%d,%d): cell unknown and no candidate.\n",i,j)
      end
      state = FINISH
      conflict = %t
      continue
    end
    //
    // Solve the current state
    if ( verbose ) then
      mprintf ("Strategy #%d: %s...\n",state,index2name(state))
    end
    before = sum(L)
    select state
    case NAKED_SINGLE
      [X,C,L] = sudoku_findnakedsubset ( X , C , L , 1 , %f , verbose )
    case HIDDEN_SINGLE
      [X,C,L] = sudoku_findhiddensubset ( X , C , L , 1 , %f , verbose )
    case LOCKED
      [X,C,L] = sudoku_findlocked ( X , C , L , %t , verbose )
    case NAKED_PAIR
      [X,C,L] = sudoku_findnakedsubset ( X , C , L , 2 , %t , verbose )
    case HIDDEN_PAIR
      [X,C,L] = sudoku_findhiddensubset ( X , C , L , 2 , %t , verbose )
    case NAKED_TRIPLE
      [X,C,L] = sudoku_findnakedsubset ( X , C , L , 3 , %t , verbose )
    case HIDDEN_TRIPLE
      [X,C,L] = sudoku_findhiddensubset ( X , C , L , 3 , %t , verbose )
    case NAKED_QUAD
      [X,C,L] = sudoku_findnakedsubset ( X , C , L , 4 , %t , verbose )
    case HIDDEN_QUAD
      [X,C,L] = sudoku_findhiddensubset ( X , C , L , 4 , %t , verbose )
    case XWING
      [X,C,L] = sudoku_findxwing ( X , C , L , %t , verbose )
    case BICOLOR
      [X,C,L] = sudoku_findtwocolors ( X , C , L , %t , verbose )
    case XCYCLE
      [X,C,L] = sudoku_findxcycle ( X , C , L , %t , verbose )
    else
      error ( msprintf ( gettext ( "%s: Unexpected state %d"),state))
    end
    if ( before <> sum(L) ) then
      iter(state) = iter(state) + 1
    end
    if ( and(X(:) > 0) ) then
      // There is no unknown anymore: we are done.
      state = FINISH
    elseif ( before <> sum(L) ) then
      // The number of candidates has been reduced: try NAKED_SINGLE again.
      state = NAKED_SINGLE
    else
      // The current strategy failed: try the next one.
      state = state + 1
    end
  end
endfunction


// Returns the (i,j) index given the global index s
// Note: in Scilab, entries are numbered column by column.
//  whatcell ( 1 ) = (1,1)
//  whatcell ( 2 ) = (2,1)
//  whatcell ( 3 ) = (3,1)
//  ...
//  whatcell ( 9 ) = (9,1)
//  whatcell ( 10 ) = (1,2)
//  whatcell ( 12 ) = (2,2)
function [i,j] = whatcell ( s )
  i = modulo(s-1,9) + 1
  j = (s-i)/9 + 1
endfunction

