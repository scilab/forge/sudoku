// Copyright (C) 2010 - 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function carray = sudoku_candidatesmerge ( X , C , L , kind , i , j , len )
  //   Merge candidates in the row, column or block.
  //
  // Calling Sequence
  //   carray = sudoku_candidatesmerge ( X , C , L , kind , i , j , len )
  //
  // Parameters
  // X: a 9-by-9 matrix, with 0 for unknown entries
  // C: a 9 x 9 cell of candidates
  // L: the 9 x 9 matrix of number candidates
  // kind: set kind=1 for row, kind=2 for column and kind=3 for block
  // i: the row of the value. If kind = 1, i in 1:9. If kind = 3, i in [1 4 7].
  // j: the column of the value. If kind = 2, j in 1:9. If kind = 3, j in [1 4 7].
  // len: an array of two reals
  // len(1): minimum number of candidates in the cell.
  // len(2): maximum number of candidates in the cell.
  // carray: a row vector containing all the unique candidates in the row, column or block.
  //
  // Description
  //   Merge all candidates in a row, column or block and returns them.
  //   Only cells which number of candidates is between lenmin and lenmax are 
  //   taken into account.
  //   Returns only unique candidates: duplicate candidates are removed.
  //   If there is no candidate in this row, column or block, returns an empty matrix.
  //   The merged candidates are returned as a row matrix.
  //
  // Examples
  // X = [
  // 4 0 0   3 9 0   0 0 2
  // 2 6 0   0 5 8   3 9 0
  // 5 9 3   6 0 0   1 8 0
  // ..
  // 1 0 0   8 6 0   0 0 9
  // 6 0 5   9 0 0   2 0 0 
  // 0 3 9   2 4 5   0 1 6
  // ..
  // 0 5 6   0 0 9   0 2 0
  // 0 1 4   7 0 0   9 0 5
  // 9 0 0   5 3 0   0 0 0
  // ];
  // [C,L] = sudoku_candidates(X);
  // sudoku_candidatesmerge ( X , C , L , 1 , 2 , [] ); // Merge all candidates from row 2
  //
  // Authors
  //   Michael Baudin, 2010
  //
  
  [lhs, rhs] = argn()
  apifun_checkrhs ( "sudoku_candidateremove" , rhs , 7 )
  apifun_checklhs ( "sudoku_candidateremove" , lhs , 1 )
  //
  // Check type
  apifun_checktype ( "sudoku_candidateremove" , X , "X" , 1 , "constant" )
  apifun_checktype ( "sudoku_candidateremove" , C , "C" , 2 , "hypermat" )
  apifun_checktype ( "sudoku_candidateremove" , L , "L" , 3 , "constant" )
  apifun_checktype ( "sudoku_candidateremove" , kind , "kind" , 4 , "constant" )
  apifun_checktype ( "sudoku_candidateremove" , i , "i" , 5 , "constant" )
  apifun_checktype ( "sudoku_candidateremove" , j , "j" , 6 , "constant" )
  apifun_checktype ( "sudoku_candidateremove" , len , "len" , 7 , "constant" )
  //
  // Check size
  apifun_checkdims ( "sudoku_candidateremove" , X , "X" , 1 , [9 9] )
  apifun_checkdims ( "sudoku_candidateremove" , C , "C" , 2 , [9 9 9] )
  apifun_checkdims ( "sudoku_candidateremove" , L , "L" , 3 , [9 9] )
  apifun_checkdims ( "sudoku_candidateremove" , kind , "kind" , 4 , [1 1] )
  if ( or(kind == [1 3 4]) ) then
    apifun_checkdims ( "sudoku_candidateremove" , i , "i" , 5 , [1 1] )
  else
    apifun_checkdims ( "sudoku_candidateremove" , i , "i" , 5 , [0 0] )
  end
  if ( or(kind == [2 3 4]) ) then
    apifun_checkdims ( "sudoku_candidateremove" , j , "j" , 6 , [1 1] )
  else
    apifun_checkdims ( "sudoku_candidateremove" , j , "j" , 6 , [0 0] )
  end
  apifun_checkvector ( "sudoku_candidateremove" , len , "len" , 7 , 2 )
  //
  carray = []
  m = 1
  if ( kind == 1 ) then
    // Merge candidates in row i
    if ( i == [] ) then
      error ( mprintf("%s: Empty i is not expected when kind = 1.","sudoku_candidatesmerge"))
    end
    for c = 1 : 9
      if ( X(i,c) == 0 & L(i,c) >= len(1) & L(i,c) <= len(2) ) then
        carray(m:m+L(i,c)-1) = find(C(i,c,:))'
        m = m + L(i,c)
      end
    end
  elseif ( kind == 2 ) then
    // Merge candidates in column j
    if ( j == [] ) then
      error ( mprintf("%s: Empty j is not expected when kind = 2.","sudoku_candidatesmerge"))
    end
    for r = 1 : 9
      if ( X(r,j) == 0 & L(r,j) >= len(1) & L(r,j) <= len(2) ) then
        carray(m:m+L(r,j)-1) = find(C(r,j,:))'
        m = m + L(r,j)
      end
    end
  elseif ( kind == 3 ) then
    // Merge candidates in block (i,j)
    if ( i == [] ) then
      error ( mprintf("%s: Empty i is not expected when kind = 3.","sudoku_candidatesmerge"))
    end
    if ( j == [] ) then
      error ( mprintf("%s: Empty j is not expected when kind = 3.","sudoku_candidatesmerge"))
    end
    for r = tri(i)
      for c = tri(j)
        if ( X(r,c) == 0 & L(r,c) >= len(1) & L(r,c) <= len(2) ) then
          carray(m:m+L(r,c)-1) = find(C(r,c,:))'
          m = m + L(r,c)
        end
      end
    end
  else
    errmsg = msprintf(gettext("%s: Unexpected value of kind = %d. Only kind = 1, 2 and 3 are managed."), "sudoku_candidatesmerge", kind);
    error(errmsg)
  end
  // Keep only unique candidates
  carray = unique(carray)
  carray = carray(:)'
endfunction

//
// tri --
//   tri(1) = [1 2 3], tri(2) = [1 2 3], tri(3) = [1 2 3]
//   tri(4) = [4 5 6], tri(5) = [4 5 6], tri(6) = [4 5 6]
//   tri(7) = [7 8 9], tri(8) = [7 8 9], tri(9) = [7 8 9]
function y = tri ( k )
  y = 3*ceil(k/3-1) + (1:3)
endfunction

