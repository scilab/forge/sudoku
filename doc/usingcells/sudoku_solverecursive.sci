function [X, maxlevel] = sudoku_solverecursive ( varargin )
  //   Solves Sudoku using naked singles and recursive backtracking.
  //
  // Calling Sequence
  //   Y = sudoku_solverecursive ( X )
  //   Y = sudoku_solverecursive ( X , verbose )
  //   Y = sudoku_solverecursive ( X , verbose , level )
  //   [Y,maxlevel] = sudoku_solverecursive ( ... )
  //
  // Parameters
  // X: a 9-by-9 matrix, with 0 for unknown entries
  // verbose: a boolean. Set to %t to display the state of the matrix. (default = %f)
  // level : the current level of recursive backtracking
  // maxlevel: the maximum level of the call tree in the nested recursive search used in the guess-based strategy
  //
  // Description
  //   Using Recursive Backtracking.
  //   From an algorithm published in The Mathworks News and Notes, 2009.
  // 
  //   There are several main improvements over the original algorithm.
  //   The matrix of candidates used in the logic process searching for naked singles
  //   is updated instead of being re-computed each time.
  //   When guessing is necessary, we do not take the first empty cell,
  //   but chose the cell with the lowest number of candidates.
  //   When we browse the candidates, we permute randomly the candidates
  //   so that the routine can be used to generate random grids.
  //   
  //
  // Examples
  // X = [
  // 0 0 0   0 0 0   0 0 0
  // 0 0 0   0 0 0   5 3 0
  // 9 0 0   0 3 7   0 0 0
  // 0 0 2   3 0 1   0 9 6
  // 0 4 7   6 9 0   1 8 0
  // 0 0 6   7 8 5   0 0 0
  // 0 5 0   0 2 0   9 0 3
  // 0 3 0   9 0 0   6 0 8
  // 8 0 0   0 1 0   4 7 0
  // ]
  // sudoku_solverecursive(X)
  //
  // X = [
  // 0 0 0   0 0 0   0 0 0
  // 9 0 0   0 0 4   0 3 6
  // 0 0 0   0 7 2   0 0 9
  // 3 0 0   0 0 0   0 0 5
  // 0 1 4   0 0 0   8 0 0
  // 0 2 5   0 1 0   0 0 0
  // 0 0 6   1 0 0   0 0 3
  // 0 0 0   0 0 6   0 4 0
  // 0 0 2   4 8 9   0 0 0
  // ]
  // sudoku_solverecursive(X)
  //
  // X = [
  // 1 2 0    0 3 0    0 4 0  
  // 6 0 0    0 0 0    0 0 3  
  // 3 0 4    0 0 0    5 0 0  
  // 2 0 0    8 0 6    0 0 0  
  // 8 0 0    0 1 0    0 0 6  
  // 0 0 0    7 0 5    0 0 0  
  // 0 0 7    0 0 0    6 0 0  
  // 4 0 0    0 0 0    0 0 8  
  // 0 3 0    0 4 0    0 2 0  
  // ]
  // sudoku_solverecursive(X)
  //
  // X = [
  // 0 2 0   0 3 0   0 4 0
  // 6 0 0   0 0 0   0 0 3
  // 0 0 4   0 0 0   5 0 0
  // 0 0 0   8 0 6   0 0 0
  // 8 0 0   0 1 0   0 0 6
  // 0 0 0   7 0 5   0 0 0
  // 0 0 7   0 0 0   6 0 0
  // 4 0 0   0 0 0   0 0 8
  // 0 3 0   0 4 0   0 2 0
  // ]
  // sudoku_solverecursive(X)
  //
  // Authors
  //   Cleve Moler, 2009
  //   Michael Baudin, Scilab port, 2010
  
  [lhs,rhs]=argn();
  if ( rhs < 1 | rhs > 3 ) then
    errmsg = msprintf(gettext("%s: Unexpected number of input arguments : %d provided while 1 or 3 are expected."), "sudoku_solverecursive", rhs);
    error(errmsg)
  end
  
  X = varargin(1)
  if ( rhs < 2 ) then
    verbose = %f
  else
    verbose = varargin(2)
  end
  if ( rhs < 3 ) then
    level = 0
  else
    level = varargin(3)
  end
  
  apifun_typereal ( X , "X" , 1 )
  apifun_typeboolean ( verbose , "verbose" , 2 )
  apifun_typereal ( level , "level" , 3 )
  
  //
  // Initialize the maxlevel
  maxlevel = level
  //
  // Solve with naked singles.
  [C,L] = sudoku_candidates ( X )
  while ( %t )
    if ( verbose ) then
      mprintf("Eliminating naked singles (remaining %d unknowns)\n",sum(X==0))
    end
    before = sum(L)
    [X,C,L] = sudoku_findnakedsubset ( X , C , L , 1 , %f , verbose )
    if ( sum(L) == before ) then
      break
    end
  end
  // Return for impossible puzzles.
  e = findfirst(X==0 & L==0)
  if ( e <> [] ) then
    if ( verbose ) then
      mprintf("Impossible puzzle !\n")
    end
    return
  end
  
  if ( and(X(:) > 0) ) then
    return
  end
  
  // 
  // Recursive backtracking. 
  Y = X 
  // The cell which provides the lowest number of candidates
  cmin = mincandidates ( X , L )
  [imin,jmin] = whatcell ( cmin )
  // Iterate over candidates. 
  for r = grand(1,"prm",C(cmin).entries)' 
    if ( verbose ) then
      mprintf("[%d] Guessing %d at (%d,%d) (remaining %d unknowns)\n",level,r,imin,jmin,sum(X==0))
    end
    X = Y
    // Insert a tentative value.
    X(cmin) = r
    // Recursive call. 
    [X,Gmaxlevel] = sudoku_solverecursive ( X , verbose , level + 1 )
    maxlevel = max( [Gmaxlevel level] )
    if and(X(:) > 0) then
      // Found a solution.
      return 
    end 
  end 
endfunction

function i = findfirst ( A )
  i = find( A )
  if ( i <> [] ) then
    i = i(1)
  end
endfunction

// Returns the (i,j) index given the global index s
// Note: in Scilab, entries are numbered column by column.
//  whatcell ( 1 ) = (1,1)
//  whatcell ( 2 ) = (2,1)
//  whatcell ( 3 ) = (3,1)
//  ...
//  whatcell ( 9 ) = (9,1)
//  whatcell ( 10 ) = (1,2)
//  whatcell ( 12 ) = (2,2)
function [i,j] = whatcell ( s )
  i = modulo(s-1,9) + 1
  j = (s-i)/9 + 1
endfunction
// Generates an error if the given variable is not of type boolean
function apifun_typeboolean ( var , varname , ivar )
  if ( type ( var ) <> 4 ) then
    errmsg = msprintf(gettext("%s: Expected boolean but for variable %s at input #%d, got %s instead."),"apifun_typeboolean", varname , ivar , typeof(var) );
    error(errmsg);
  end
endfunction
// Generates an error if the given variable is not of type real
function apifun_typereal ( var , varname , ivar )
  if ( type ( var ) <> 1 ) then
    errmsg = msprintf(gettext("%s: Expected real variable for variable %s at input #%d, but got %s instead."),"apifun_typereal", varname , ivar , typeof(var) );
    error(errmsg);
  end
endfunction

// Returns the index of the unknown cell with the minimum number of candidates
function k = mincandidates ( X , L )
  nbc = %inf
  k = []
  for i = find(X == 0 )
    if ( L(i) < nbc ) then
      k =  i
      nbc = L(i)
    end
  end
endfunction

