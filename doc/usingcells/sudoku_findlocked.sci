function [X,C,L] = sudoku_findlocked ( varargin )
  //   Search for locked candidates.
  //
  // Calling Sequence
  //   [X,C,L] = sudoku_findlocked ( X , C , L )
  //   [X,C,L] = sudoku_findlocked ( X , C , L , stopatfirst )
  //   [X,C,L] = sudoku_findlocked ( X , C , L , stopatfirst , verbose )
  //
  // Parameters
  // X: a 9-by-9 matrix, with 0 for unknown entries
  // C: a cell array of candidate vectors for each cell.
  // L: the number of candidates for each cell
  // stopatfirst: if %t, then stop after one or more candidates have been removed. (default = %t)
  // verbose: a boolean. Set to %t to display the state of the matrix. (default = %f)
  //
  // Description
  //   Search for locked candidates of types 1 and 2 and eliminates them.
  //   
  //   The type 1 algorithm searches in blocks (i.e. minigrids)
  //   for each occurence of the numbers n=1,2,...,9.
  //   It may happen that one candidate n appears in one single 
  //   row or column. In this case, the candidate can be eliminated
  //   from other blocks in the same row or column.
  //   This is often called "Locked Candidate" of type 1 (Pointing).
  //
  //   Locked candidates of type 2 (Claiming) are also searched.
  //   The algorithm performs a loop for n=1,2,...,9 and for each 
  //   row i. If there are cells which contains n as a candidate
  //   and are in one single block, we remove n from the candidates 
  //   of the other cells in the same block.
  //   We also search for locked candidates of type 2 in columns.
  //
  // Examples
  // X = [
  // 1 0 8   6 7 2   0 9 3
  // 0 0 9   8 1 4   6 0 2
  // 0 6 0   9 5 3   8 0 1
  // ..
  // 0 0 6   0 0 7   1 8 0
  // 0 2 1   0 9 8   7 3 6
  // 0 8 0   1 0 6   0 0 0
  // ..
  // 0 1 4   3 8 9   0 6 7
  // 6 0 0   0 4 1   9 0 8
  // 8 9 0   0 6 5   3 1 4
  // ]
  // [C,L] = sudoku_candidates(X) 
  // [X,C,L] = sudoku_findlocked ( X , C , L , %f , %t ) // 2 is eliminated as candidate in (6,5)
  //
  // Authors
  //   Michael Baudin, 2010
  //
  // Bibliography
  //   The mathematics of Sudoku, Tom Davis, 2008
  //   http://hodoku.sourceforge.net/en/tech_intersections.php#lc1
  
  [lhs,rhs]=argn();
  if ( rhs < 3 | rhs > 5 ) then
    errmsg = msprintf(gettext("%s: Unexpected number of input arguments : %d provided while 3 to 5 are expected."), "sudoku_findlocked", rhs);
    error(errmsg)
  end
  X = varargin(1)
  C = varargin(2)
  L = varargin(3)
  if ( rhs < 4 ) then
    stopatfirst = %t
  else
    stopatfirst = varargin(4)
  end
  if ( rhs < 5 ) then
    verbose = %f
  else
    verbose = varargin(5)
  end
  
  apifun_typereal ( X , "X" , 1 )
  apifun_typecell ( C , "C" , 2 )
  apifun_typereal ( L , "L" , 3 )
  apifun_typeboolean ( stopatfirst , "stopatfirst" , 4 );
  apifun_typeboolean ( verbose , "verbose" , 5 );
  
  before = sum(L)
  
  // Search for locked candidates of type 1
  // Search in blocks
  for i = [1 4 7]
    trii = tri(i)
    for j = [1 4 7]
      trij = tri(j)
      for n = 1 : 9
        // Search for n in the block (i,j).
        rows = []
        cols = []
        for r = trii
          for c = trij
            z = find ( C(r,c).entries == n )
            if ( z <> [] ) then
              rows ( $+1 ) = r
              cols ( $+1 ) = c
            end
          end
        end
        if ( rows <> [] & and(rows==rows(1)) ) then
          // If n appears only in one row, remove n from all cells
          // of row r except in the block (i,j)
          firstverb = %t
          r = rows(1)
          for c = 1:trij(1)-1
            if ( X(r,c) == 0 & find(C(r,c).entries==n) <> [] ) then
              [ C , L ] = sudoku_candidateremove ( C , L , 4 , r , c , n )
              if ( verbose ) then
                if ( firstverb ) then
                  mprintf("Found row locked candidate (type 1) %d in block (%d,%d)\n",n,i,j)
                  firstverb = %f
                end
                mprintf("Removed row locked candidate (type 1) %d at (%d,%d) (remaining %d candidates)\n",n,r,c,sum(L))
              end
            end
          end
          for c = trij(3)+1:9
            if ( X(r,c) == 0 & find(C(r,c).entries==n) <> [] ) then
              [ C , L ] = sudoku_candidateremove ( C , L , 4 , r , c , n )
              if ( verbose ) then
                if ( firstverb ) then
                  mprintf("Found row locked candidate (type 1) %d in block (%d,%d)\n",n,i,j)
                  firstverb = %f
                end
                mprintf("Removed row locked candidate (type 1) %d at (%d,%d) (remaining %d candidates)\n",n,r,c,sum(L))
              end
            end
          end
        elseif ( cols <> [] & and(cols==cols(1)) ) then
          // If n appears only in one column, remove n from all cells
          // of column c except in the block (i,j)
          firstverb = %t
          c = cols(1)
          for r = 1:trii(1)-1
            if ( X(r,c) == 0 & find(C(r,c).entries==n) <> [] ) then
              [ C , L ] = sudoku_candidateremove ( C , L , 4 , r , c , n )
              if ( verbose ) then
                if ( firstverb ) then
                  mprintf("Found column locked candidate (type 1) %d in block (%d,%d)\n",n,i,j)
                  firstverb = %f
                end
                mprintf("Removed column locked candidate (type 1) %d at (%d,%d) (remaining %d candidates)\n",n,r,c,sum(L))
              end
            end
          end
          for r = trii(3)+1:9
            if ( X(r,c) == 0 & find(C(r,c).entries==n) <> [] ) then
              [ C , L ] = sudoku_candidateremove ( C , L , 4 , r , c , n )
              if ( verbose ) then
                if ( firstverb ) then
                  mprintf("Found column locked candidate (type 1) %d in block (%d,%d)\n",n,i,j)
                  firstverb = %f
                end
                mprintf("Removed column locked candidate (type 1) %d at (%d,%d) (remaining %d candidates)\n",n,r,c,sum(L))
              end
            end
          end
        end
        if ( stopatfirst & sum(L) <> before ) then
          return
        end
      end
    end
  end
  
  // Search for locked candidates of type 2
  // Search in rows
  for n = 1 : 9
    for i = 1 : 9
      // Search for columns containing n as a candidate
      cols = zeros(1,9)
      for j = 1 : 9
        if ( X(i,j) == 0 & find(C(i,j).entries==n)<> [] ) then
          cols(j) = j
        end
      end
      cols(find(cols==0)) = []
      if ( cols == [] ) then
        // n is not a candidate in this row : look in the next row
        continue
      end
      // See if the columns are in one single block
      inblock = 0
      for j = [1 4 7]
        trij = tri(j)
        minj = min(trij)
        maxj = max(trij)
        if ( and(cols>=minj) & and(cols<=maxj) ) then
          inblock = j
          break
        end
      end
      if ( inblock == 0 ) then
        // No locked candidate here: try next
        continue
      end
      firstverb = %t
      // There are locked candidates in row i, block j
      for r = tri(i)
        for c = tri(inblock)
          if ( r <> i & find(C(r,c).entries==n) <> [] ) then
            [ C , L ] = sudoku_candidateremove ( C , L , 4 , r , c , n )
            if ( verbose ) then
              if ( firstverb ) then
                mprintf("Found row locked candidate (type 2) %d in row %d\n",n,i)
                firstverb = %f
              end
              mprintf("Remove row locked candidate (type 2) %d at (%d,%d) (remaining %d candidates)\n",n,r,c,sum(L))
            end
          end
        end
      end
      if ( stopatfirst & sum(L) <> before ) then
        return
      end
    end
  end
  
  // Search for locked candidates of type 2
  // Search in columns
  for n = 1 : 9
    for j = 1 : 9
      // Search for rows containing n as a candidate
      rows = zeros(1,9)
      for i = 1 : 9
        if ( X(i,j) == 0 & find(C(i,j).entries==n)<> [] ) then
          rows(i) = i
        end
      end
      rows(find(rows==0)) = []
      if ( rows == [] ) then
        // n is not a candidate in this column : look in the next column
        continue
      end
      // See if the rows are in one single block
      inblock = 0
      for i = [1 4 7]
        trii = tri(i)
        min_i = min(trii)
        max_i = max(trii)
        if ( and(rows>=min_i) & and(rows<=max_i) ) then
          inblock = i
          break
        end
      end
      if ( inblock == 0 ) then
        // There is no locked candidate in column j: try next
        continue
      end
      
      // There are locked candidates in block i
      firstverb = %t
      for r = tri(inblock)
        for c = tri(j)
          if ( c <> j & find(C(r,c).entries==n) <> [] ) then
            [ C , L ] = sudoku_candidateremove ( C , L , 4 , r , c , n )
            if ( verbose ) then
              if ( firstverb ) then
                mprintf("Found column locked candidate (type 2) %d in column %d\n",n,j)
                firstverb = %f
              end
              mprintf("Remove column locked candidate (type 2) %d at (%d,%d) (remaining %d candidates)\n",n,r,c,sum(L))
            end
          end
        end
      end
      if ( stopatfirst & sum(L) <> before ) then
        return
      end
    end
  end
  
endfunction

// Generates an error if the given variable is not of type boolean
function apifun_typeboolean ( var , varname , ivar )
  if ( type ( var ) <> 4 ) then
    errmsg = msprintf(gettext("%s: Expected boolean but for variable %s at input #%d, got %s instead."),"apifun_typeboolean", varname , ivar , typeof(var) );
    error(errmsg);
  end
endfunction
// Generates an error if the given variable is not of type real
function apifun_typereal ( var , varname , ivar )
  if ( type ( var ) <> 1 ) then
    errmsg = msprintf(gettext("%s: Expected real variable for variable %s at input #%d, but got %s instead."),"apifun_typereal", varname , ivar , typeof(var) );
    error(errmsg);
  end
endfunction
// Generates an error if the given variable is not of type cell
function apifun_typecell ( var , varname , ivar )
  if ( typeof ( var ) <> "ce" ) then
    errmsg = msprintf(gettext("%s: Expected cell variable for variable %s at input #%d, but got %s instead."),"apifun_typecell", varname , ivar , typeof(var) );
    error(errmsg);
  end
endfunction

//http://www.sudocue.net/guide.php#LockedCandidates
//http://www.angusj.com/sudoku/hints.php

// tri --
//   tri(1) = [1 2 3], tri(2) = [1 2 3], tri(3) = [1 2 3]
//   tri(4) = [4 5 6], tri(5) = [4 5 6], tri(6) = [4 5 6]
//   tri(7) = [7 8 9], tri(8) = [7 8 9], tri(9) = [7 8 9]
function y = tri ( k )
  y = 3*ceil(k/3-1) + (1:3)
endfunction
// nonzeros --
// Returns the values in A which are nonzero.
//   nonzeros([0 1 0 2 3 4]) = [1 2 3 4]
//   nonzeros([0 0 0 4]) = 4
function s = nonzeros ( A )
  [i,j,s] = mtlb_find( A )
endfunction

