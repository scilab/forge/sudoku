function [X,C,L] = sudoku_findhiddensingles ( varargin )
  //   Fills necessary entries.
  //
  // Calling Sequence
  //   [X,C,L] = sudoku_findhiddensingles ( X , C , L )
  //   [X,C,L] = sudoku_findhiddensingles ( X , C , L , verbose )
  //
  // Parameters
  // X: a 9-by-9 matrix, with 0 for unknown entries
  // C: a 9 x 9 cell of candidates
  // L: the 9 x 9 matrix of number candidates
  // verbose: a boolean. Set to %t to display the state of the matrix. (default = %f)
  //
  // Description
  //   Search for "lone rangers" in the sudoku X, given the 
  //   candidates in C. Returns an updated matrix X.
  //   Hidden singles (i.e. "Lone rangers") are those cells which are associated 
  //   with several candidates: one of these candidates
  //   cannot possibly be in another row, column or minigrid.
  //   This is in Chapter 4, "Intermediate techniques" in 
  //   "Programming Sudoku".
  //   This algorithm is superseded by findhiddensubset.
  //
  // Examples
  // X = [
  // 0 0 0   0 0 0   0 0 0
  // 0 0 0   0 0 0   5 3 0
  // 9 0 0   0 3 7   0 0 0
  // ..
  // 0 0 2   3 0 1   0 9 6
  // 0 4 7   6 9 0   1 8 0
  // 0 0 6   7 8 5   0 0 0
  // ..
  // 0 5 0   0 2 0   9 0 3
  // 0 3 0   9 0 0   6 0 8
  // 8 0 0   0 1 0   4 7 0
  // ]
  // [C,L] = sudoku_candidates(X);
  // sudoku_findhiddensingles ( X , C , L , %t )
  //
  // X = [
  // 5 8 3   0 2 0   0 9 0 
  // 0 0 0   5 9 3   2 0 0 
  // 4 9 2   0 0 0   0 0 0 
  // ..
  // 0 0 5   7 0 0   1 0 0 
  // 0 6 7   0 0 0   5 3 0 
  // 0 0 8   0 5 9   0 0 0 
  // ..
  // 0 0 0   0 0 5   0 1 2 
  // 0 0 9   2 0 6   0 0 0 
  // 2 5 0   0 8 0   0 6 7 
  // ];
  // [C,L] = sudoku_candidates(X);
  // [X,C,L] = sudoku_findhiddensingles ( X , C , L , %t );
  //
  // Authors
  //   Michael Baudin, 2010
  //
  // Bibliography
  //   Programming Sudoku, Wei-Meng Lee, 2006
  
  [lhs,rhs]=argn();
  if ( rhs < 3 | rhs > 4 ) then
    errmsg = msprintf(gettext("%s: Unexpected number of input arguments : %d provided while 3 or 4 are expected."), "sudoku_findhiddensingles", rhs);
    error(errmsg)
  end
  X = varargin(1)
  C = varargin(2)
  L = varargin(3)
  if ( rhs < 4 ) then
    verbose = %f
  else
    verbose = varargin(4)
  end
  apifun_typereal ( X , "X" , 1 )
  apifun_typecell ( C , "C" , 2 )
  apifun_typereal ( L , "L" , 3 )
  apifun_typeboolean ( verbose , "verbose" , 4 );

  // Look for Hidden Singles in Rows
  for i = 1 : 9
    for n = 1 : 9
      // See if n is a hidden single in the row i
      occurence = 0
      for j = find(X(i,:)==0)
          f = find(C(i,j).entries==n)
          if ( f <> [] ) then
            occurence = occurence + 1
            if ( occurence > 1 ) then
              // The integer n can't be a hidden single, check next n.
              break
            end
            irow = i
            jcol = j
          end
      end
      if ( occurence == 1 ) then
        // n is a hidden single
        if ( verbose ) then
          mprintf("Found hidden single in row at (%d,%d) as %d\n",irow,jcol,n)
        end
        [ X , C , L ] = sudoku_confirmcell ( X , C , L , irow , jcol , n , verbose )
      end
    end
  end
  // Look for Hidden Singles in Columns
  for j = 1 : 9
    for n = 1 : 9
      // See if n is a hidden single in the column j
      occurence = 0
      for i = find(X(:,j) == 0)
          f = find(C(i,j).entries==n)
          if ( f <> [] ) then
            occurence = occurence + 1
            if ( occurence > 1 ) then
              // The integer n can't be a hidden single, check next n.
              break
            end
            irow = i
            jcol = j
          end
      end
      if ( occurence == 1 ) then
        // n is a hidden single
        if ( verbose ) then
          mprintf("Confirmed hidden single in column at (%d,%d) as %d\n",irow,jcol,n)
        end
        [ X , C , L ] = sudoku_confirmcell ( X , C , L , irow , jcol , n , verbose )
      end
    end
  end
  // Look for Hidden Singles in Minigrids
  for n = 1 : 9
    for i = [1 4 7]
      for j = [1 4 7]
        nextMinigrid = %f
        // See if n is a hidden single in the Minigrid (i,j)
        occurence = 0
        for s = find(X(tri(i),tri(j)) == 0)
          [rr,cc] = whatcellinblock ( s )
          rr = rr + i - 1
          cc = cc + j - 1
          f = find(C(rr,cc).entries==n)
          if ( f <> [] ) then
            occurence = occurence + 1
            if ( occurence > 1 ) then
              // The integer n can't be a hidden single, check next n.
              nextMinigrid = %t
              break
            end
            irow = rr
            jcol = cc
          end
        end
        if ( occurence == 1 & ~nextMinigrid ) then
          // n is a hidden single
          if ( verbose ) then
            mprintf("Confirmed hidden single in block at (%d,%d) as %d\n",irow,jcol,n)
          end
          [ X , C , L ] = sudoku_confirmcell ( X , C , L , irow , jcol , n , verbose )
        end
      end
    end
  end
endfunction

// Generates an error if the given variable is not of type boolean
function apifun_typeboolean ( var , varname , ivar )
  if ( type ( var ) <> 4 ) then
    errmsg = msprintf(gettext("%s: Expected boolean but for variable %s at input #%d, got %s instead."),"apifun_typeboolean", varname , ivar , typeof(var) );
    error(errmsg);
  end
endfunction
// Generates an error if the given variable is not of type real
function apifun_typereal ( var , varname , ivar )
  if ( type ( var ) <> 1 ) then
    errmsg = msprintf(gettext("%s: Expected real variable for variable %s at input #%d, but got %s instead."),"apifun_typereal", varname , ivar , typeof(var) );
    error(errmsg);
  end
endfunction
// Generates an error if the given variable is not of type cell
function apifun_typecell ( var , varname , ivar )
  if ( typeof ( var ) <> "ce" ) then
    errmsg = msprintf(gettext("%s: Expected cell variable for variable %s at input #%d, but got %s instead."),"apifun_typecell", varname , ivar , typeof(var) );
    error(errmsg);
  end
endfunction


// Returns the (i,j) index given the global index s in a block
// Note: in Scilab, entries are numbered column bX column.
//  whatcellinblock ( 1 ) = (1,1)
//  whatcellinblock ( 2 ) = (2,1)
//  whatcellinblock ( 3 ) = (3,1)
//  whatcellinblock ( 4 ) = (1,2)
//  ...
//  whatcellinblock ( 9 ) = (9,1)
function [i,j] = whatcellinblock ( s )
  i = modulo(s-1,3) + 1
  j = (s-i)/3 + 1
endfunction

function X = sudoku_findhiddsin2 ( varargin )

  
  [lhs,rhs]=argn();
  if ( rhs < 2 | rhs > 3 ) then
    errmsg = msprintf(gettext("%s: Unexpected number of input arguments : %d provided while 2 or 3 are expected."), "sudoku_findhiddensingles", rhs);
    error(errmsg)
  end
  X = varargin(1)
  C = varargin(2)
  if ( rhs < 3 ) then
    verbose = %f
  else
    verbose = varargin(3)
  end

  X = X
  // Look for Hidden Singles in Rows
  for i = 1 : 9
    for n = 1 : 9
      // See if n is a hidden single in the row i
      occurence = 0
      for j = 1 : 9
        if ( X(i,j) == 0 ) then
          f = find(C(i,j).entries==n)
          if ( f <> [] ) then
            occurence = occurence + 1
            if ( occurence > 1 ) then
              // The integer n can't be a hidden single, check next n.
              break
            end
            irow = i
            jcol = j
          end
        end
      end
      if ( occurence == 1 ) then
        // n is a hidden single
        X(irow,jcol) = n
        if ( verbose ) then
          mprintf("Confirmed hidden single in (%d,%d) as %d\n",irow,jcol,n)
        end
      end
    end
  end
  // Look for Hidden Singles in Columns
  for j = 1 : 9
    for n = 1 : 9
      // See if n is a hidden single in the column j
      occurence = 0
      for i = 1 : 9
        if ( X(i,j) == 0 ) then
          f = find(C(i,j).entries==n)
          if ( f <> [] ) then
            occurence = occurence + 1
            if ( occurence > 1 ) then
              // The integer n can't be a hidden single, check next n.
              break
            end
            irow = i
            jcol = j
          end
        end
      end
      if ( occurence == 1 ) then
        // n is a hidden single
        X(irow,jcol) = n
        if ( verbose ) then
          mprintf("Confirmed hidden single in (%d,%d) as %d\n",irow,jcol,n)
        end
      end
    end
  end
  // Look for Hidden Singles in Minigrids
  for n = 1 : 9
    for i = [1 4 7]
      for j = [1 4 7]
        nextMinigrid = %f
        // See if n is a hidden single in the Minigrid (i,j)
        occurence = 0
        for rr = tri(i)
          for cc = tri(j)
            if ( X(rr,cc) == 0 ) then
              f = find(C(rr,cc).entries==n)
              if ( f <> [] ) then
                occurence = occurence + 1
                if ( occurence > 1 ) then
                  // The integer n can't be a hidden single, check next n.
                  nextMinigrid = %t
                  break
                end
                irow = rr
                jcol = cc
              end
            end
          end
          if ( nextMinigrid ) then
            break
          end
        end
        if ( occurence == 1 & ~nextMinigrid ) then
          // n is a hidden single
          X(irow,jcol) = n
          if ( verbose ) then
            mprintf("Confirmed hidden single in (%d,%d) as %d\n",irow,jcol,n)
          end
        end
      end
    end
  end
endfunction

function [X,C,L] = sudoku_findhiddsin3 ( varargin )
  //
  // This is an alternative implementation.
  // It is slower 
  //
  // Authors
  // Stefan Bleeck, 2005
  // Michael Baudin, Scilab port, refactoring, comments, 2010
  

  [lhs,rhs]=argn();
  if ( rhs < 3 | rhs > 4 ) then
    errmsg = msprintf(gettext("%s: Unexpected number of input arguments : %d provided while 3 or 4 are expected."), "sudoku_findhiddensingles", rhs);
    error(errmsg)
  end
  
  X = varargin(1)
  C = varargin(2)
  L = varargin(3)
  if ( rhs < 4 ) then
    verbose = %f
  else
    verbose = varargin(4)
  end
  
  // the number we are looking at
  for v = 1:9
    // cross out all the cells that are impossible by entering ones in them
    // initially none is crossed out
    crossed = zeros(9,9)  
    
    fv = find(X > 0)
    if ( fv <> [] ) then
      // cross out those already filled
      crossed(fv) = 1 
    end 
    for i = 1:9
      if ( find(X(i,:) == v) <> [] ) then
        // cross out row
        crossed(i,:) = 1 
      end 
    end
    for j = 1 : 9
      if ( find(X(:,j) == v) <> [] ) then
        // cross out column
        crossed(:,j) = 1 
      end
    end
    
    for i = [1,4,7]
      for j = [1,4,7]
        trii = tri(i)
        trij = tri(j)
        if ( find(X(trii,trij) == v) <> [] ) then
          // cross out sub-square
          crossed(trii,trij) = 1 
        end
      end
    end
    
    for i = [1,4,7]
      for j = [1,4,7]
        trii = tri(i)
        trij = tri(j)
        if ( find(X(trii,trij) == v) == [] & sum(crossed(trii,trij)) == 8 ) then
          // this sub-square will contain it
          // only one possible cell
          // find it
          [ii,jj] = find(crossed(trii,trij) == 0)
          r = i+ii-1
          c = j+jj-1
          if ( verbose ) then
            mprintf("Confirmed necessary (%d,%d) as %d\n",r,c,v)
          end
          [ X , C , L ] = sudoku_confirmcell ( X , C , L , r , c , v , verbose )
        end
      end
    end
  end
  
endfunction



