function [X,C,L] = sudoku_findnakedtriples ( varargin )
  //   Find triples.
  //
  // Calling Sequence
  //   [X,C,L] = sudoku_findnakedtriples ( X , C , L )
  //   [X,C,L] = sudoku_findnakedtriples ( X , C , L , stopatfirst )
  //   [X,C,L] = sudoku_findnakedtriples ( X , C , L , stopatfirst , verbose )
  //
  // Parameters
  // X: a 9-by-9 matrix, with 0 for unknown entries
  // C: a 9 x 9 cell of candidates
  // L: the 9 x 9 matrix of number candidates
  // stopatfirst: if %t, then stop when one or more candidates have been removed. (default = %t)
  // verbose: a boolean. Set to %t to display the state of the matrix. (default = %f)
  //
  // Description
  //   Search for naked triples.
  //   This algorithm is superseded by findnakedsubset.
  //
  //   The algorithm computes a merged list of all candidates in the row, column
  //   or block. This merged list is simplified and only unique candidates are kept.
  //   The list is used to generate all combinations of 3 elements.
  //   Each combination S is searched in the row, column or block.
  //   The row, column or block contains a naked triple if exactly three 
  //   cells exactly have candidates all contained in S.
  //   If a naked triple is found, these candidates are eliminated from the other cells in the 
  //   row, column or block.
  // 
  //   A simplified version of this algorithm is in Chapter 5, "Intermediate techniques" in 
  //   "Advanced Techniques".
  //   The algorithm of Lee does not find triples of the form {a,b}, {b,c}, {a,c}.
  //   This algorithm does.
  //
  //
  // Examples
  // X = [
  // 4 0 0   3 9 0   0 0 2
  // 2 6 0   0 5 8   3 9 0
  // 5 9 3   6 0 0   1 8 0
  // ..
  // 1 0 0   8 6 0   0 0 9
  // 6 0 5   9 0 0   2 0 0 
  // 0 3 9   2 4 5   0 1 6
  // ..
  // 0 5 6   0 0 9   0 2 0
  // 0 1 4   7 0 0   9 0 5
  // 9 0 0   5 3 0   0 0 0
  // ];
  // [C,L] = sudoku_candidates(X);
  // sudoku_findnakedtriples ( X , C , L , %f , %t ); // Found triple {1,7,8} in row 1 at (1,2), (1,3), (1,6).
  //
  // Authors
  //   Michael Baudin, 2010
  //
  // Bibliography
  //   Programming Sudoku, Wei-Meng Lee, 2006
  
  [lhs,rhs]=argn();
  if ( rhs < 3 | rhs > 5 ) then
    errmsg = msprintf(gettext("%s: Unexpected number of input arguments : %d provided while 3 to 5 are expected."), "sudoku_findnakedtriples", rhs);
    error(errmsg)
  end
  X = varargin(1)
  C = varargin(2)
  L = varargin(3)
  if ( rhs < 4 ) then
    stopatfirst = %t
  else
    stopatfirst = varargin(4)
  end
  if ( rhs < 5 ) then
    verbose = %f
  else
    verbose = varargin(5)
  end
  
  apifun_typereal ( X , "X" , 1 )
  apifun_typecell ( C , "C" , 2 )
  apifun_typereal ( L , "L" , 3 )
  apifun_typeboolean ( stopatfirst , "stopatfirst" , 4 );
  apifun_typeboolean ( verbose , "verbose" , 5 );
  
  before = sum(L)

  // Search for triples in rows
  for i = 1 : 9
    // Search for triples in row i.
    // Assemble all candidates of the row.
    carray  = sudoku_candidatesmerge ( X , C , L , 1 , i , [] , [2 3] )
    if ( carray == [] ) then
      // There are no possible triple in this row, go on to the next
      continue
    end
    n = size(carray,"*")
    if ( n <= 2 ) then
      // There are only 1 or 2 unique candidates in this row:
      // there cannot exist a triple here, try the next row.
      continue
    end
    // Get all possible combinations of 3 elements among 
    // these candidates
    cmap = combine ( carray , 3 )
    nk = size(cmap,"r")
    // Search for each combinations in the row i
    for m = 1 : nk
      S = cmap(m,:)
      icols = []
      for c = 1 : 9
        if ( X(i,c) == 0 & ( L(i,c)==2 | L(i,c)==3 ) ) then
          if ( areAllContained ( C(i,c).entries' , S ) ) then
            // This might be a triple: register it
            icols($+1) = c
          end
        end
      end
      if ( size(icols,"*") <> 3 ) then
        // This triple is not represented in the row:
        // try next triple.
        continue
      end
      // This is a triple: remove the candidates 
      // from the other cells in the row.
      first = %t
      for c = 1 : 9
        if ( X(i,c) == 0 & and(c <> icols) ) then
          // This is a cell which does not belong to the triple
          // remove the triple from the candidates
          for v = S
            if ( find ( C(i,c).entries == v ) <> [] ) then
              [ C , L ] = sudoku_candidateremove ( C , L , 4 , i , c , v )
              if ( verbose ) then
                if ( first ) then
                  mprintf("Found naked triple {%d,%d,%d} in row %d at (%d,%d), (%d,%d), (%d,%d)\n" , ..
                    S(1),S(2),S(3),i,i,icols(1),i,icols(2),i,icols(3))
                  first = %f
                end
                mprintf("Removed triple row candidate %d at (%d,%d) (remaining %d candidates)\n",v,i,c,sum(L))
              end
            end
          end
        end
      end
      if ( stopatfirst & sum(L) <> before ) then
        return
      end
    end
  end
  
  // Search for triples in columns
  for j = 1 : 9
    // Search for triples in column j.
    // Assemble all candidates of the column.
    carray  = sudoku_candidatesmerge ( X , C , L , 2 , [] , j , [2 3] )
    if ( carray == [] ) then
      // There are no possible triple in this column, go on to the next
      continue
    end
    n = size(carray,"*")
    if ( n <= 2 ) then
      // There are only 1 or 2 unique candidates in this column:
      // there cannot exist a triple here, try the next column.
      continue
    end
    // Get all possible combinations of 3 elements among 
    // these candidates.
    cmap = combine ( carray , 3 )
    nk = size(cmap,"r")
    // Search for each combinations in the column i
    for m = 1 : nk
      S = cmap(m,:)
      irows = []
      for r = 1 : 9
        if ( X(r,j) == 0 & ( L(r,j)==2 | L(r,j)==3 ) ) then
          if ( areAllContained ( C(r,j).entries' , S ) ) then
            // This might be a triple: register it
            irows($+1) = r
          end
        end
      end
      if ( size(irows,"*") <> 3 ) then
        // This triple is not represented in the column:
        // try next triple.
        continue
      end
      // This is a triple: remove the candidates 
      // from the other cells in the column.
      first = %t
      for r = 1 : 9
        if ( X(r,j) == 0 & and(r <> irows) ) then
          // This is a cell which does not belong to the triple
          // remove the triple from the candidates.
          for v = S
            if ( find ( C(r,j).entries == v ) <> [] ) then
              [ C , L ] = sudoku_candidateremove ( C , L , 4 , r , j , v )
              if ( verbose ) then
                if ( first ) then
                  mprintf("Found naked triple {%d,%d,%d} in column %d at (%d,%d), (%d,%d), (%d,%d)\n" , ..
                    S(1),S(2),S(3),j,irows(1),j,irows(2),j,irows(3),j)
                  first = %f
                end
                mprintf("Removed triple column candidate %d at (%d,%d) (remaining %d candidates)\n",v,r,j,sum(L))
              end
            end
          end
        end
      end
      if ( stopatfirst & sum(L) <> before ) then
        return
      end
    end
  end
  // Search for triples in blocks
  for i = [1 4 7]
    for j = [1 4 7]
      // Search for triples in block (i,j)
      // Assemble all candidates of the block
      carray  = sudoku_candidatesmerge ( X , C , L , 3 , i , j , [2 3] )
      if ( carray == [] ) then
        // There are no possible triple in this block, go on to the next
        continue
      end
      n = size(carray,"*")
      if ( n <= 2 ) then
        // There are only 1 or 2 unique candidates in this block:
        // there cannot exist a triple here, try the next block.
        continue
      end
      // Get all possible combinations of 3 elements among 
      // these candidates
      cmap = combine ( carray , 3 )
      nk = size(cmap,"r")
      // Search for each combinations in the block (i,j)
      for m = 1 : nk
        S = cmap(m,:)
        irows = []
        icols = []
        for r = tri(i)
          for c = tri(j)
            if ( X(r,c) == 0 & ( L(r,c)==2 | L(r,c)==3 ) ) then
              if ( areAllContained ( C(r,c).entries' , S ) ) then
                // This might be a triple: register it
                irows($+1) = r
                icols($+1) = c
              end
            end
          end
        end
        if ( size(irows,"*") <> 3 ) then
          // This triple is not represented in the block
          // Try next triple
          continue
        end
        // This is a triple: remove the candidates 
        // from the other cells in the block
        first = %t
        for r = tri(i)
          for c = tri(j)
            if ( X(r,c) == 0 & and(~(r == irows & c == icols)) ) then
              // This is a cell which does not belong to the triple
              // remove the triple from the candidates
              for v = S
                if ( find ( C(r,c).entries == v ) <> [] ) then
                  [ C , L ] = sudoku_candidateremove ( C , L , 4 , r , c , v )
                  if ( verbose ) then
                    if ( first ) then
                      mprintf("Found naked triple {%d,%d,%d} in block (%d,%d) at (%d,%d), (%d,%d), (%d,%d)\n" , ..
                        S(1),S(2),S(3),i,j,irows(1),icols(1),irows(2),icols(2),irows(3),icols(3))
                      first = %f
                    end
                    mprintf("Removed triple block candidate %d at (%d,%d) (remaining %d candidates)\n",v,r,c,sum(L))
                  end
                end
              end
            end
          end
        end
        if ( stopatfirst & sum(L) <> before ) then
          return
        end
      end
    end
  end
  
endfunction

// Generates an error if the given variable is not of type boolean
function apifun_typeboolean ( var , varname , ivar )
  if ( type ( var ) <> 4 ) then
    errmsg = msprintf(gettext("%s: Expected boolean but for variable %s at input #%d, got %s instead."),"apifun_typeboolean", varname , ivar , typeof(var) );
    error(errmsg);
  end
endfunction
// Generates an error if the given variable is not of type real
function apifun_typereal ( var , varname , ivar )
  if ( type ( var ) <> 1 ) then
    errmsg = msprintf(gettext("%s: Expected real variable for variable %s at input #%d, but got %s instead."),"apifun_typereal", varname , ivar , typeof(var) );
    error(errmsg);
  end
endfunction
// Generates an error if the given variable is not of type cell
function apifun_typecell ( var , varname , ivar )
  if ( typeof ( var ) <> "ce" ) then
    errmsg = msprintf(gettext("%s: Expected cell variable for variable %s at input #%d, but got %s instead."),"apifun_typecell", varname , ivar , typeof(var) );
    error(errmsg);
  end
endfunction


// Returns the (i,j) index given the global index s in a block
// Note: in Scilab, entries are numbered column bX column.
//  whatcellinblock ( 1 ) = (1,1)
//  whatcellinblock ( 2 ) = (2,1)
//  whatcellinblock ( 3 ) = (3,1)
//  whatcellinblock ( 4 ) = (1,2)
//  ...
//  whatcellinblock ( 9 ) = (9,1)
function [i,j] = whatcellinblock ( s )
  i = modulo(s-1,3) + 1
  j = (s-i)/3 + 1
endfunction

// combine --
// Returns all combinations of k values from the row vector x as a row-by-row array
// with (n,k) rows where (n,k) is the binomial coefficient and n is the 
// number of values in x. 
//
// References
// http://home.att.net/~srschmitt/script_combinations.html
// Kenneth H. Rosen, Discrete Mathematics and Its Applications, 2nd edition (NY: McGraw-Hill, 1991), pp. 284-286.
//
// Example
//   combine ( [17 32 48 53] , 3 ) :
//       17.    32.    48.  
//       17.    32.    53.  
//       17.    48.    53.  
//       32.    48.    53.  
//
function cmap = combine ( x , k )
  n = size(x,"*")
  if ( n < k ) then
    error ( msprintf ( "%s: The number of values in x is n = %d which is lower than k = %d\n", n,k))
  end
  c = combinations ( n , k )
  cmap = zeros(c,k)
  a = 1:k
  cmap(1,:) = x(a)
  for m = 2 : c
    i = k
    while ( a(i) == n - k + i )
      i = i - 1
    end
    a(i) = a(i) + 1
    for j = i+1 : k
      a(j) = a(i) + j - i
    end
    cmap(m,:) = x(a)
  end
endfunction
//
// whatcell --
// Returns the (i,j) index given the global index s
// Note: in Scilab, entries are numbered column by column.
//
// Example
//  whatcell ( 1 ) = (1,1)
//  whatcell ( 2 ) = (2,1)
//  whatcell ( 3 ) = (3,1)
//  ...
//  whatcell ( 9 ) = (9,1)
//  whatcell ( 10 ) = (1,2)
//  whatcell ( 12 ) = (2,2)
function [i,j] = whatcell ( s )
  i = modulo(s-1,9) + 1
  j = (s-i)/9 + 1
endfunction

// areAllContained --
// Returns %t if all the items in the row vector x are in the 
// row set S.
//
// Example 
//   areAllContained ( [3 5 7] , 1:2:11 ) is true
//   areAllContained ( [2 4 7] , 2:2:10 ) is false
function bool = areAllContained ( x , S )
  bool = %t
  for v = x
    if ( find(S==v) == [] ) then
      bool = %f
      break
    end
  end
endfunction

// combinations --
//   Returns the number of combinations of j objects chosen from n objects.
//   If the input where integers, returns also an integer.
function c = combinations ( n , j )
  c = exp(gammaln(n+1)-gammaln(j+1)-gammaln(n-j+1));
  if ( and(round(n)==n) & and(round(j)==j) ) then
    c = round ( c )
  end
endfunction

