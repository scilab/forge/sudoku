function str = sudoku_candidateprint ( X , C , L , d )
  //   Print the locations where a given candidate is found.
  //
  // Calling Sequence
  //   str = sudoku_candidateprint ( X , C , L , d )
  //
  // Parameters
  // X: a 9-by-9 matrix, with 0 for unknown entries
  // C: a 9 x 9 cell of candidates
  // L: the 9 x 9 matrix of number candidates
  // d: the digit
  // str: the string
  //
  // Description
  //   Some algorithms, like X-Cycles, X-wings and Swordfish focus on a particular
  //   candidate digit. This function allows to display the locations where a given digit
  //   is located as a candidate.
  //
  // Examples
  // 
  // X = [
  // 0 2 4   1 0 0   6 7 0 
  // 0 6 0   0 7 0   4 1 0 
  // 7 0 0   9 6 4   0 2 0 
  // ..
  // 2 4 6   5 9 1   3 8 7 
  // 1 3 5   4 8 7   2 9 6 
  // 8 7 9   6 2 3   1 5 4 
  // ..
  // 4 0 0   0 0 9   7 6 0 
  // 3 5 0   7 1 6   9 4 0 
  // 6 9 7   0 4 0   0 3 1 
  // ];
  // [C,L] = sudoku_candidates(X);
  // sudoku_candidateprint ( X , C , L , 8 );
  //
  // Authors
  //   Michael Baudin, 2010
  //
  // Bibliography
  //   Angus Johnson, http://www.angusj.com/sudoku/hints.php
  
  
  apifun_typereal ( X , "X" , 1 )
  apifun_typehypermat ( C , "C" , 2 )
  apifun_typereal ( L , "L" , 3 )
  apifun_typereal ( d , "d" , 4 )

    // Get all cells, ordered by column, containg d as a candidate
    [rows,cols] = sudoku_candidatefind ( X , C , L , d )
  //
  // Create a matrix of strings
  len = size(rows,"*")
  S(1:9,1:9) = "";
  for k = 1 : len
    S(rows(k),cols(k)) = string(k)
  end
  //
  // Compute the maximum number of digits in a cell
  lmax = 0
  for k = 1 : len
    lmax = max ( [length(S(rows(k),cols(k))) lmax ] )
  end
  // Add a blank at the end
  lmax = lmax + 1
  //
  // Update the matrix of string and pad with blanks up to the max
  for i = 1 : 9
    for j = 1 : 9
      lS = length(S(i,j))
      if ( lS < lmax ) then
        S(i,j) = S(i,j) + blanks(lmax - lS)
      end
    end
  end
  //
  // Create a matrix of rows suitable for human reading
  str = []
  k = 0
  for i = 1 : 9
    k = k + 1
    str(k) = ""
    for j = 1 : 9
      str(k) = str(k) + S(i,j)
      if ( modulo(j,3) == 0 & j < 9 ) then
        str(k) = str(k) + "|"
      end
    end
    if ( modulo(i,3) == 0 & i < 9 ) then
      k = k + 1
      str(k) = ""
      for km = 1 : lmax*9 + 3
        str(k) = str(k) + "-"
      end
    end
  end
  //
  // Print the rows
  for r = 1 : size(str,"r")
    mprintf("%s\n",str(r))
  end
endfunction

