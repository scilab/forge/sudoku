// Copyright (C) - Michael Baudin - 2010

// Explore the possibility to use Knuth's Dancing Link to solve sudoku in Scilab.

// http://fr.wikipedia.org/wiki/Probl%C3%A8me_de_la_couverture_exacte
// http://www.osix.net/modules/article/?id=792
// http://code.google.com/p/narorumo/wiki/SudokuDLX
// http://www.stolaf.edu/people/hansonr/sudoku/exactcovermatrix.htm

// The Sudoku is an exact cover problem represented by a sparse matrix
// made of zeros and ones. This matrix has:
// * 729 rows, one for each candidate in each cell,
// * 324 columns, one for each constraint.
//
// Each of the 729 rows represent a candidate in a cell.
// There are 9*9=81 cells and each cell may contain
// 9 candidates. This makes 9*9*9=729 total candidates.
//
// Each of the 324 columns represent a constraint.
// There are 4 types of constraints.
// 1: each cell can only contain one digit (from 1 to 9),
// 2: in one row, each cell can only contain one digit (from 1 to 9),
// 3: in one column, each cell can only contain one digit (from 1 to 9),
// 4: in one block, each cell can only contain one digit (from 1 to 9).
// There are 81 constraints of type #1, because there are 81 cells in the sudoku.
// There are 81 constraints of type #2, because in each of the 9 rows, each of the 9 cells
// has one constraint of this type.
// There are 81 constraints of type #3, because in each of the 9 columns, each of the 9 cells
// has one constraint of this type.
// There are 81 constraints of type #4, because in each of the 9 blocks, each of the 9 cells
// has one constraint of this type.
// This makes 81+81+81+81=324 columns.
//
// The exact cover problem associated with the sudoku consists in finding
// in the 729*324 matrix, a set of 81 rows where each column contains exactly one 1.
//

// Is that possible to display a 729 * 324 matrix in Scilab ?

nrows = 729
ncols = 324
C = zeros(nrows,ncols);
for i = 1 : nrows
  mprintf("%s\n",strcat(string(C(i,:))," "))
end
// It is not nice, but we can do it !
// http://en.wikipedia.org/wiki/Knuth's_Algorithm_X
// http://arxiv.org/abs/cs/0011047


