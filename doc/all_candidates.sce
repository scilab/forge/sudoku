// Copyright (C) 2010 - Michael Baudin

// Display all possible candidates
  for i = 1 : 9
    mprintf("1 2 3  1 2 3  1 2 3  |  1 2 3  1 2 3  1 2 3 |  1 2 3  1 2 3  1 2 3\n")
    mprintf("4 5 6  4 5 6  4 5 6  |  4 5 6  4 5 6  4 5 6 |  4 5 6  4 5 6  4 5 6\n")
    mprintf("7 8 9  7 8 9  7 8 9  |  7 8 9  7 8 9  7 8 9 |  7 8 9  7 8 9  7 8 9\n")
    if ( modulo(i,3) == 0 & i <> 9 ) then
      mprintf("-------------------------------------------------------------------\n")
    else
      mprintf("\n")
    end
  end

