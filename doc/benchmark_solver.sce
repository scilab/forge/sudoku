// Copyright (C) - Michael Baudin - 2010


// Measure the performance of the solver
X = [
0 0 0   0 0 0   0 0 0 
0 0 0   0 0 0   0 0 0 
0 0 0   0 0 0   0 0 0 
..
0 0 0   0 0 0   0 0 0 
0 0 0   0 0 0   0 0 0 
0 0 0   0 0 0   0 0 0 
..
0 0 0   0 0 0   0 0 0 
0 0 0   0 0 0   0 0 0 
0 0 0   0 0 0   0 0 0 
];
tic();
Y = sudoku_solve(X);
toc()

// Find optimum parameters
function y = myfunction (p)
  mprintf("p=[%s]\n",strcat(string(p)," "))
  tic();
  X = sudoku_solve(X,%f,p);
  y = toc();
  mprintf("time=%f\n",y)
endfunction

if ( %f ) then
// Hardest grid
  X = [
  0 0 0   0 0 0   0 0 0
  0 0 0   0 0 0   0 0 0
  0 0 0   0 0 0   0 0 0
  ..
  0 0 0   0 0 0   0 0 0
  0 0 0   0 0 0   0 0 0
  0 0 0   0 0 0   0 0 0
  ..
  0 0 0   0 0 0   0 0 0
  0 0 0   0 0 0   0 0 0
  0 0 0   0 0 0   0 0 0
  ];
end
if ( %f ) then
// An medium grid
X = [
0 0 0   0 0 0   0 0 0 
0 0 0   0 0 0   5 3 0 
9 0 0   0 3 7   0 0 0 
..
0 0 2   3 0 1   0 9 6 
0 0 0   0 0 0   0 0 0
0 0 6   7 8 5   0 0 0 
..
0 5 0   0 2 0   9 0 3 
0 0 0   0 0 0   0 0 0
8 0 0   0 1 0   4 7 0 
];
end
// An easy grid
X = [
0 0 0   0 0 0   0 0 0 
0 0 0   0 0 0   5 3 0 
9 0 0   0 3 7   0 0 0 
..
0 0 2   3 0 1   0 9 6 
0 4 7   6 9 0   1 8 0 
0 0 6   7 8 5   0 0 0 
..
0 5 0   0 2 0   9 0 3 
0 3 0   9 0 0   6 0 8 
8 0 0   0 1 0   4 7 0 
];

p0 = [81 81 0]
opt = optimset ( "Display" , "iter" );
[x, fval, exitflag, output] = fminsearch ( myfunction , p0 , opt );

///////////////////////////////////////////////////////////////////////

    
// We produce a neighbor by adding some noise to each component of a given vector
function x_neigh = neigh_func (x_current, T, saparams)
  sa_min_delta = -10*ones(x_current)
  sa_max_delta = 10*ones(x_current)
  x_neigh = x_current + (sa_max_delta - sa_min_delta).*rand(size(x_current,1),size(x_current,2)) + sa_min_delta
  x_neigh = round(x_neigh)
  x_neigh = min(x_neigh,82*ones(x_current))
  x_neigh = max(x_neigh,zeros(x_current))
endfunction


//p0 = [82 82 0 0]
p0 = [0 0 0 0 0]
Proba_start = 0.7;
It_Pre      = 5;
It_extern   = 5;
It_intern   = 5;

saparams = init_param();
saparams = add_param(saparams,'neigh_func', neigh_func);

T0 = compute_initial_temp(p0, myfunction, Proba_start, It_Pre, saparams);

Log = %T
[x_opt , f_opt , sa_mean_list, sa_var_list] = optim_sa(p0, myfunction, It_extern, It_intern, T0, Log , saparams);

printf('optimal solution:\n'); disp(x_opt);
printf('value of the objective function = %f\n', f_opt);

t = 1:length(sa_mean_list);
plot(t,sa_mean_list,'r',t,sa_var_list,'g');

///////////////////////////////////////////////////////////////////////
X = [
0 0 0   0 0 0   0 0 0 
0 0 0   0 0 0   5 3 0 
9 0 0   0 3 7   0 0 0 
..
0 0 2   3 0 1   0 9 6 
0 4 7   6 9 0   1 8 0 
0 0 6   7 8 5   0 0 0 
..
0 5 0   0 2 0   9 0 3 
0 3 0   9 0 0   6 0 8 
8 0 0   0 1 0   4 7 0 
];
add_profiling("sudoku_fillnecessary")
X = sudoku_fillnecessary (X);
plotprofile(sudoku_fillnecessary)

add_profiling("sudoku_necessary")
N = sudoku_necessary (X);
plotprofile(sudoku_necessary)

add_profiling("sudoku_solve")
X = sudoku_solve (X);
plotprofile(sudoku_solve)

///////////////////////////////////////////////////////////////////////
// Optimize the parameters one by one.
for i = 0:10:80
  t = myfunction ([i 0 0 0 0]);
  mprintf("t=%f\n",t);
end

for i = 0:10:80
  t = myfunction ([0 i 0 0 0]);
  mprintf("t=%f\n",t);
end

for i = 0:10:80
  t = myfunction ([0 0 i 0 0]);
  mprintf("t=%f\n",t);
end

for i = 0:10:80
  t = myfunction ([0 0 i 0 0]);
  mprintf("t=%f\n",t);
end

for i = 0:10:80
  t = myfunction ([0 0 0 i 0]);
  mprintf("t=%f\n",t);
end

for i = 0:10:80
  t = myfunction ([0 0 0 0 i]);
  mprintf("t=%f\n",t);
end


