// Copyright (C) - Michael Baudin - 2010

// Creating Latin Square of size n
// http://en.wikipedia.org/wiki/Latin_square

function X = latinsquare ( n )
  X = zeros(n,n)
  for i = 1 : n
    X(i,:) = modulo((1:n)+(i-2),n) + 1
  end
  r = grand(1,"prm",(1:n)')'
  X(:,r) = X(:,:)
  r = grand(1,"prm",(1:n)')
  X(r,:) = X(:,:)
endfunction

