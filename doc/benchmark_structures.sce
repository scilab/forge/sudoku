// Copyright (C) 2010 - Michael Baudin

// Compare data structures to store the matrix of candidates

function [C,L] = sudoku_candidatesHM(X) 
//   Returns candidates for a sudoku X.
  C = zeros(9,9,9)
  L = zeros(9,9)
  for s = find(X==0)
    [i,j] = whatcell ( s )
    d = 1:9
    zone = X(i,:)
    d(zone(find(zone>0))) = 0
    zone = X(:,j)
    d(zone(find(zone>0))) = 0
    zone = X(tri(i),tri(j))
    d(zone(find(zone>0))) = 0
    C(i,j,:) = d
    L(i,j) = size(find(d>0),"*")
  end 
endfunction
function [C,L] = sudoku_candidatesCell ( X ) 
  C = cell(9,9)
  L = zeros(9,9)
  for s = find(X==0)
    [i,j] = whatcell ( s )
    d = 1:9
    zone = X(i,:)
    d(zone(find(zone>0))) = 0
    zone = X(:,j)
    d(zone(find(zone>0))) = 0
    zone = X(tri(i),tri(j))
    d(zone(find(zone>0))) = 0
    C(i,j).entries = d(find(d>0))'
    L(i,j) = size(find(d>0),"*")
  end 
endfunction
function [C,L] = sudoku_candidatesHM2(X) 
//   Returns candidates for a sudoku X.
  C(1:9,1:9,1:9) = %t
  [irows,icols] = find(X>0)
  nf = size(irows,"*")
  for k = 1 : nf
    r = irows(k); c = icols(k)
    d = X(r,c)
    C(r,:,d) = %f;
    C(:,c,d) = %f;
    C(tri(r),tri(c),d) = %f;
    C(r,c,:) = %f;
  end
  L = zeros(9,9)
  for r = 1 : 9
    for c = 1 : 9
      L(r,c) = size(find(C(r,c,:)),"*")
    end
  end
endfunction

function sudoku_printC2 ( X )  
  S = []
  for i = 1 : 9
    for j = 1 : 9
      nz = find(C(i,j,:))
      if ( nz == [] ) then
        S(i,j) = ""
      else
        snz = size(nz,"*")
        cs = matrix(nz,snz,1)
        S(i,j) = msprintf("{%s}",strcat(string(cs)," "))
      end
    end
  end
  disp(S)  
endfunction

function [C,L] = sudoku_candidatesHM3(X) 
//   Returns candidates for a sudoku X.
  C(1:9,1:9,1:9) = %t
  [irows,icols] = find(X>0)
  nf = size(irows,"*")
  for k = 1 : nf
    r = irows(k); c = icols(k)
    d = X(r,c)
    C(d,r,:) = %f;
    C(d,:,c) = %f;
    C(d,tri(r),tri(c)) = %f;
    C(:,r,c) = %f;
  end
  L = zeros(9,9)
  for r = 1 : 9
    for c = 1 : 9
      L(r,c) = size(find(C(:,r,c)),"*")
    end
  end
endfunction

function sudoku_printC3 ( X )  
  S = []
  for i = 1 : 9
    for j = 1 : 9
      nz = find(C(:,i,j))
      if ( nz == [] ) then
        S(i,j) = ""
      else
        snz = size(nz,"*")
        cs = matrix(nz,snz,1)
        S(i,j) = msprintf("{%s}",strcat(string(cs)," "))
      end
    end
  end
  disp(S)  
endfunction


// tri --
//   tri(1) = [1 2 3], tri(2) = [1 2 3], tri(3) = [1 2 3]
//   tri(4) = [4 5 6], tri(5) = [4 5 6], tri(6) = [4 5 6]
//   tri(7) = [7 8 9], tri(8) = [7 8 9], tri(9) = [7 8 9]
function y = tri ( k )
  y = 3*ceil(k/3-1) + (1:3)
endfunction

// nonzeros --
// Returns the values in A which are nonzero.
//   nonzeros([0 1 0 2 3 4]) = [1 2 3 4]
//   nonzeros([0 0 0 4]) = 4
function s = nonzeros ( A )
  [i,j,s] = mtlb_find( A )
endfunction
// Returns the (i,j) index given the global index s
// Note: in Scilab, entries are numbered column by column.
//  whatcell ( 1 ) = (1,1)
//  whatcell ( 2 ) = (2,1)
//  whatcell ( 3 ) = (3,1)
//  ...
//  whatcell ( 9 ) = (9,1)
//  whatcell ( 10 ) = (1,2)
//  whatcell ( 12 ) = (2,2)
function [i,j] = whatcell ( s )
  i = modulo(s-1,9) + 1
  j = (s-i)/9 + 1
endfunction



// Test cells with row vectors
// Fill randomly the candidates
C = cell ( 9 , 9 );
for i = 1 : 9
  for j = 1 : 9
    z = grand(1,"prm",(1:9)')';
    k = ceil ( grand(1,1,"unf",0,1) * 9 );
    C(i,j).entries = z(1:k);
  end
end
// Print :
C
// Measure the access time to the list of candidates of all cells
timer();
for i = 1 : 9
  for j = 1 : 9
    z = C(i,j).entries;
  end
end
t = timer();
mprintf("Cell (row) - Fill random - CPU Time = %f\n",t);


// Test cells with column vectors
// Fill randomly the candidates
C = cell ( 9 , 9 );
for i = 1 : 9
  for j = 1 : 9
    z = grand(1,"prm",(1:9)')';
    k = ceil ( grand(1,1,"unf",0,1) * 9 );
    C(i,j).entries = z(1:k)';
  end
end
// Print :
C

timer();
for i = 1 : 9
  for j = 1 : 9
    z = C(i,j).entries;
  end
end
t = timer();
mprintf("Cell (column) - Fill random - CPU Time = %f\n",t);

// Test hypermatrix with 0 for unknown entries
// Fill randomly the candidates
C = zeros ( 9 , 9 , 9 );
for i = 1 : 9
  for j = 1 : 9
    z = grand(1,"prm",(1:9)')';
    k = ceil ( grand(1,1,"unf",0,1) * 9 );
    for d = z(1:k)
      C(i,j,d) = d;
    end
  end
end
// Printing is less easy...
  S = [];
  for i = 1 : 9
    for j = 1 : 9
      nz = find(C(i,j,:)>0);
      if ( nz == [] ) then
        S(i,j) = "";
      else
        snz = size(nz,"*");
        cs = matrix(C(i,j,nz),snz,1);
        S(i,j) = msprintf("{%s}",strcat(string(cs)," "));
      end
    end
  end
  disp(S)  

timer();
for i = 1 : 9
  for j = 1 : 9
    z = find(C(i,j,:)>0);
  end
end
t = timer();
mprintf("Hypermatrix - Fill random - CPU Time = %f\n",t);

// Compare the computation of the candidates
X = [
0 0 0   0 0 0   0 0 0 
0 0 0   0 0 0   5 3 0 
9 0 0   0 3 7   0 0 0 
..
0 0 2   3 0 1   0 9 6 
0 4 7   6 9 0   1 8 0 
0 0 6   7 8 5   0 0 0 
..
0 5 0   0 2 0   9 0 3 
0 3 0   9 0 0   6 0 8 
8 0 0   0 1 0   4 7 0 
];


timer();
[C,L] = sudoku_candidatesHM ( X );
t = timer();
mprintf("Compute Candidates - HM - CPU Time = %f\n",t);


timer();
[C,L] = sudoku_candidatesCell ( X );
t = timer();
mprintf("Compute Candidates - Cell - CPU Time = %f\n",t);

timer();
[C,L] = sudoku_candidatesHM2(X);
t = timer();
mprintf("Compute Candidates - HM-2 - CPU Time = %f\n",t);
//sudoku_printC2 ( C )

timer();
[C,L] = sudoku_candidatesHM3(X);
t = timer();
mprintf("Compute Candidates - HM-3 - CPU Time = %f\n",t);
sudoku_printC3 ( C )

add_profiling("sudoku_candidatesHM");
[C,L] = sudoku_candidatesHM ( X );
plotprofile(sudoku_candidatesHM);

add_profiling("sudoku_candidatesCell");
[C,L] = sudoku_candidatesCell ( X );
plotprofile(sudoku_candidatesCell);

add_profiling("sudoku_candidatesHM2");
[C,L] = sudoku_candidatesHM2 ( X );
plotprofile(sudoku_candidatesHM2);

X = sudoku_readgivens("...............53.9...37.....23.1.96.4769.18...6785....5..2.9.3.3.9..6.88...1.47.");
sudoku_solve ( X );
add_profiling("sudoku_solve");
[Y,iter] = sudoku_solve ( X );
plotprofile(sudoku_solve);

X = sudoku_readgivens("...............53.9...37.....23.1.96.4769.18...6785....5..2.9.3.3.9..6.88...1.47.");
sudoku_solvebylogic ( X );
add_profiling("sudoku_solvebylogic");
[Y,iter] = sudoku_solvebylogic ( X );
plotprofile(sudoku_solvebylogic);

X = sudoku_readgivens("...............53.9...37.....23.1.96.4769.18...6785....5..2.9.3.3.9..6.88...1.47.");
sudoku_candidates ( X );
add_profiling("sudoku_candidates");
[C,L] = sudoku_candidates ( X );
plotprofile(sudoku_candidates);

X = sudoku_readgivens("...............53.9...37.....23.1.96.4769.18...6785....5..2.9.3.3.9..6.88...1.47.");
[C,L] = sudoku_candidates ( X );
sudoku_findnakedsubset ( X , C , L , 1 , %f , %f );
add_profiling("sudoku_findnakedsubset");
sudoku_findnakedsubset ( X , C , L , 1 , %f , %f );
plotprofile(sudoku_findnakedsubset);


X = sudoku_readgivens("...............53.9...37.....23.1.96.4769.18...6785....5..2.9.3.3.9..6.88...1.47.");
[C,L] = sudoku_candidates ( X );
sudoku_findhiddensubset ( X , C , L , 1 , %f , %f );
add_profiling("sudoku_findhiddensubset");
sudoku_findhiddensubset ( X , C , L , 1 , %f , %f );
plotprofile(sudoku_findhiddensubset);


X = sudoku_readgivens("...............53.9...37.....23.1.96.4769.18...6785....5..2.9.3.3.9..6.88...1.47.");
[C,L] = sudoku_candidates ( X );
sudoku_confirmcell ( X , C , L , 1 , 1 , 2 );
add_profiling("sudoku_confirmcell");
sudoku_confirmcell ( X , C , L , 1 , 1 , 2 );
plotprofile(sudoku_confirmcell);


