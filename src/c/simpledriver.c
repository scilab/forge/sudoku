// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <limits.h>

#include "sudoku_engine.h"
#include "simpledriver.h"

// simplifiedsolver
//   Solves one single sudoku
//
// Parameters
//
// Inputs:
// puzzle: a string containing the sudoku puzzle, with dots (i.e. ".") for unknowns
//
// Outputs:
// status: the status of the computation. 
// totalsol: the total number of solutions for this puzzle
// score: the score of the first solution, positive
// depth: the maximum depth of the recursive calls for the first puzzle. depth is greater than 1.
// sol: the first solution
//
// Description
// The following describes the status:
//   status=-1 if the format of the puzzle is inconsistent (e.g. the sudoku contains two "5" in one column)
//   status=0 if the puzzle could not been solved
//   status=1 if the puzzle has been solved
//
// The following describes the score:
// Degree of Difficulty	Score
// Trivial 	80 points or less
// Easy 	81 - 125 points
// Medium 	126 - 225 points
// Hard 	226 - 350 points
// Very Hard 	351 - 760 points
// Diabolical 	761 and up
//
void simplifiedsolver(char puzzle[82], int * status, int * totalsol, int * score, int * depth, char sol[82])
{
	int i, bogus, opt, count, solved, unsolved, solncount, explain, first_soln_only;
    int prt_count, prt_num, prt_score, prt_answer, prt_depth, prt_grid, prt_mask, prt_givens, prt;
    char *myname, *infile, *outfile, *rejectfile;
	Grid *s, *g, *solved_list;
	FILE *h, *solnfile, *rejects;

	/* Init */
	h = stdin;
	solnfile = stdout;
	rejects = stderr;
	rejectfile = infile = outfile = NULL;
	count = solved = unsolved = 0;
	explain = bogus = prt_mask = prt_grid = prt_score = prt_depth = prt_answer = prt_count = prt_num = prt_givens = 0;
	first_soln_only = 0;

    if (first_soln_only && prt_score) {
		// Scoring is meaningless when multi-solution mode is disabled
		*status=0;
		return;
    }

	if (rejectfile && !(rejects = fopen(rejectfile, "w"))) {
		// Failed to open reject output file
		*status=0;
		return;
	}

    if (outfile && !(solnfile = fopen(outfile, "w"))) {
		// Failed to open solution output file
		*status=0;
		return;
	}

	if (infile && strcmp(infile, "-") && !(h = fopen(infile, "r"))) {
       	// Failed to open input game file
		*status=0;
		return;
    }

	rejects = NULL;
	solnfile = NULL;
	init_solve_engine(NULL, solnfile, rejects, first_soln_only, explain);

	solved_list = solve_sudoku(puzzle);

	if (solved_list == NULL) {
		// Invalid puzzle format
		*status=-1;
		return;
	}
	
	*status=1;

	*totalsol= solved_list->solncount;
	
    if (solved_list->solncount) {
	    for (solncount = 0, g = s = solved_list; s; s = s->next) {
			*score = g->score;
			*depth = g->maxlvl;
			format_answer(s, sol);
			
			// We only keep the first solution:
			break;
        }
    }
	else 
	{
		// Insoluble
		*status=0;
		return;
    }

	free_soln_list(solved_list);

	return;
}
