// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->
// <-- ENGLISH IMPOSED -->



function candidates = candcell ( C , i , j )
  candidates = find(C(i,j,:))
endfunction


X = [
0 2 0   6 3 0   0 4 0 
6 0 0   0 0 0   0 0 3 
3 0 4   0 0 0   5 6 0 
..
0 0 0   8 0 6   0 0 0 
8 0 0   0 1 0   0 0 6 
0 6 0   7 0 5   0 0 0 
..
0 8 7   0 0 0   6 0 4 
4 0 0   0 6 7   0 0 8 
0 3 6   0 4 8   0 2 0 
];

[C,L] = sudoku_candidates(X);
[ X , C , L ] = sudoku_confirmcell ( X , C , L , 7 , 5 , 5 );
XE = [
0 2 0   6 3 0   0 4 0 
6 0 0   0 0 0   0 0 3 
3 0 4   0 0 0   5 6 0 
..
0 0 0   8 0 6   0 0 0 
8 0 0   0 1 0   0 0 6 
0 6 0   7 0 5   0 0 0 
..
0 8 7   0 5 0   6 0 4 
4 0 0   0 6 7   0 0 8 
0 3 6   0 4 8   0 2 0 
];
assert_checkequal ( X , XE );
// Check row 7
assert_checkequal ( candcell(C,7,1) , [1 2 9] );
assert_checkequal ( candcell(C,7,4) , [1 2 3 9] );
assert_checkequal ( candcell(C,7,6) , [1 2 3 9] );
assert_checkequal ( candcell(C,7,8) , [1 3 9] );
// Check column 5
assert_checkequal ( candcell(C,2,5) , [2 7 8 9] );
assert_checkequal ( candcell(C,3,5) , [2 7 8 9] );
assert_checkequal ( candcell(C,4,5) , [2 9] );
assert_checkequal ( candcell(C,6,5) , [2 9] );
assert_checkequal ( candcell(C,6,5) , [2 9] );
// Check block (7,4)
assert_checkequal ( candcell(C,8,4) , [1 2 3 9] );
assert_checkequal ( candcell(C,9,4) , [1 9] );
LE = [
4 0 4   0 0 2   4 0 3 
0 4 4   5 4 4   5 4 0 
0 3 0   3 4 3   0 0 4 
..
5 5 5   0 2 0   6 5 5 
0 4 4   4 0 4   5 4 0 
3 0 4   0 2 0   6 4 3 
..
3 0 0   4 0 4   0 3 0 
0 3 4   4 0 0   3 4 0 
3 0 0   2 0 0   3 0 4 
];
assert_checkequal ( L , LE );

