// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->
// <-- ENGLISH IMPOSED -->



V = sudoku_visiblefrom2 ( [1 2] , [1 9] );
E = [
%t %t %t   %t %t %t   %t %t %t 
%f %f %f   %f %f %f   %f %f %f 
%f %f %f   %f %f %f   %f %f %f 
..
%f %f %f   %f %f %f   %f %f %f 
%f %f %f   %f %f %f   %f %f %f 
%f %f %f   %f %f %f   %f %f %f 
..
%f %f %f   %f %f %f   %f %f %f 
%f %f %f   %f %f %f   %f %f %f 
%f %f %f   %f %f %f   %f %f %f 
];
assert_checkequal ( V , E );

V = sudoku_visiblefrom2 ( [1 3] , [3 1] );
E = [
%t %t %t   %f %f %f   %f %f %f 
%t %t %t   %f %f %f   %f %f %f 
%t %t %t   %f %f %f   %f %f %f 
..
%f %f %f   %f %f %f   %f %f %f 
%f %f %f   %f %f %f   %f %f %f 
%f %f %f   %f %f %f   %f %f %f 
..
%f %f %f   %f %f %f   %f %f %f 
%f %f %f   %f %f %f   %f %f %f 
%f %f %f   %f %f %f   %f %f %f 
];
assert_checkequal ( V , E );

V = sudoku_visiblefrom2 ( [8 2] , [7 8] );
E = [
%f %f %f   %f %f %f   %f %f %f 
%f %f %f   %f %f %f   %f %f %f 
%f %f %f   %f %f %f   %f %f %f 
..
%f %f %f   %f %f %f   %f %f %f 
%f %f %f   %f %f %f   %f %f %f 
%f %f %f   %f %f %f   %f %f %f 
..
%t %t %t   %f %f %f   %f %f %f 
%f %f %f   %f %f %f   %t %t %t 
%f %f %f   %f %f %f   %f %f %f 
];
assert_checkequal ( V , E );






