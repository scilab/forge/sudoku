// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->
// <-- ENGLISH IMPOSED -->


////////////////////////////////////////
// Solving by logic only is required
X = [
0 0 0   0 0 0   0 0 0 
0 0 0   0 0 0   5 3 0 
9 0 0   0 3 7   0 0 0 
..
0 0 2   3 0 1   0 9 6 
0 4 7   6 9 0   1 8 0 
0 0 6   7 8 5   0 0 0 
..
0 5 0   0 2 0   9 0 3 
0 3 0   9 0 0   6 0 8 
8 0 0   0 1 0   4 7 0 
];
[Y,iter,maxlevel] = sudoku_solve(X);
solved = sudoku_issolved(Y(1));
assert_checkequal ( solved , %t );
assert_checkequal ( iter(1) , 3 );
assert_checkequal ( maxlevel , 0 );

////////////////////////////////////////
// Logic only.
X = [
0 0 0   0 0 0   0 0 0 
9 0 0   0 0 4   0 3 6 
0 0 0   0 7 2   0 0 9 
..
3 0 0   0 0 0   0 0 5 
0 1 4   0 0 0   8 0 0 
0 2 5   0 1 0   0 0 0 
..
0 0 6   1 0 0   0 0 3 
0 0 0   0 0 6   0 4 0 
0 0 2   4 8 9   0 0 0 
];
[Y,iter,maxlevel] = sudoku_solve(X);
solved = sudoku_issolved(Y(1));
assert_checkequal ( solved , %t );
assert_checkequal ( iter(1) , 6 );
assert_checkequal ( iter(2) , 1 );
assert_checkequal ( maxlevel , 0 );
////////////////////////////////////////
// An impossible sudoku
X = [
1 2 0   0 3 0   0 4 0 
6 0 0   0 0 0   0 0 3 
3 0 4   0 0 0   5 0 0 
..
2 0 0   8 0 6   0 0 0 
8 0 0   0 1 0   0 0 6 
0 0 0   7 0 5   0 0 0 
..
0 0 7   0 0 0   6 0 0 
4 0 0   0 0 0   0 0 8 
0 3 0   0 4 0   0 2 0 
];
[Y,iter,maxlevel] = sudoku_solve(X);
assert_checkequal ( Y==list() , %t );
assert_checkequal ( iter(1) , 1 );
assert_checkequal ( maxlevel , 0 );
////////////////////////////////////////
// A difficult sudoku !
if ( %f ) then
X = [
1 2 0   0 3 0   0 4 0 
0 0 0   0 0 0   0 0 0 
0 0 0   0 0 0   0 0 0 
..
0 0 0   0 0 0   0 0 0 
0 0 0   0 0 0   0 0 0 
0 0 0   0 0 0   0 0 0 
..
0 0 0   0 0 0   0 0 0 
0 0 0   0 0 0   0 0 0 
0 0 0   0 0 0   0 0 0 
];
[Y,iter,maxlevel] = sudoku_solve(X);
solved = sudoku_issolved(Y);
assert_checkequal ( solved , %t );
end

////////////////////////////////////////
// An inconsistent sudoku

X = [
1 2 0   0 3 0   0 4 0 
3 5 0   0 0 0   0 0 0 
4 6 0   0 0 0   0 0 0 
..
2 1 0   0 0 0   0 0 0 
5 3 0   0 0 0   0 0 0 
6 0 0   0 0 0   0 0 0 
..
7 0 0   0 0 0   0 0 0 
8 0 0   0 0 0   0 0 0 
9 0 0   0 0 0   0 0 0 
];
[Y,iter,maxlevel] = sudoku_solve(X);
assert_checkequal ( Y==list() , %t );
assert_checkequal ( iter($) , 1 );
assert_checkequal ( maxlevel , 1 );

////////////////////////////////////////
// Force use of guessing on a simple example
// Guess only with one candidate: easy !
X = [
0 0 0   0 0 0   0 0 0 
0 0 0   0 0 0   5 3 0 
9 0 0   0 3 7   0 0 0 
..
0 0 2   3 0 1   0 9 6 
0 4 7   6 9 0   1 8 0 
0 0 6   7 8 5   0 0 0 
..
0 5 0   0 2 0   9 0 3 
0 3 0   9 0 0   6 0 8 
8 0 0   0 1 0   4 7 0 
];
[Y,iter,maxlevel] = sudoku_solve(X,%f,[40 %inf]);
solved = sudoku_issolved(Y(1));
assert_checkequal ( solved , %t );
assert_checkequal ( iter(1) , 2 );
assert_checkequal ( iter($) , 9 );
assert_checkequal ( maxlevel , 9 );

////////////////////////////////////////
// Search the three solutions of this sudoku
// Puzzle
// ................3.9...37.....23.1.96.4769.18...6785....5..2.9.3.3.9..6.8....1.47.
// The solutions :
// 613259847475168239928437561582341796347692185196785324754826913231974658869513472
// 673158249415269837928437561582341796347692185196785324754826913231974658869513472
// 673158249418269537925437861582341796347692185196785324754826913231974658869513472

X = [
0 0 0   0 0 0   0 0 0 
0 0 0   0 0 0   0 3 0 
9 0 0   0 3 7   0 0 0 
..
0 0 2   3 0 1   0 9 6 
0 4 7   6 9 0   1 8 0 
0 0 6   7 8 5   0 0 0 
..
0 5 0   0 2 0   9 0 3 
0 3 0   9 0 0   6 0 8 
0 0 0   0 1 0   4 7 0 
];
[SS,iter,maxlevel] = sudoku_solve(X,%f,[64 %inf],0,%inf);
assert_checkequal ( size(SS) , 3 );
assert_checkequal ( sudoku_issolved(SS(1)) , %t );
assert_checkequal ( sudoku_issolved(SS(2)) , %t );
assert_checkequal ( sudoku_issolved(SS(3)) , %t );
assert_checkequal ( maxlevel <= 2 , %t );

////////////////////////////////////////
// A puzzle with 65 solutions.
// We only try to find the 10 first solutions.
X = sudoku_readgivens(".71.....36....5....98.736.4...95.4....7...2....5.26...7.953.14....1....71.....58.");
[SS,iter,maxlevel] = sudoku_solve(X,%f,[64 %inf],0,10);
assert_checkequal ( size(SS) , 10 );

