// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->
// <-- ENGLISH IMPOSED -->


////////////////////////////////////////

X = sudoku_latinsquare(4);
islatin = sudoku_islatin(X);
assert_checkequal ( islatin , %t );

X = sudoku_latinsquare(4,%f);
islatin = sudoku_islatin(X);
assert_checkequal ( islatin , %t );

