// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->
// <-- ENGLISH IMPOSED -->


function candidates = candcell ( C , i , j )
  candidates = find(C(i,j,:))
endfunction

////////////////////////////////////////


X = [
1 0 8   6 7 2   0 9 3
0 0 9   8 1 4   6 0 2
0 6 0   9 5 3   8 0 1
..
0 0 6   0 0 7   1 8 0
0 2 1   0 9 8   7 3 6
0 8 0   1 0 6   0 0 0
..
0 1 4   3 8 9   0 6 7
6 0 0   0 4 1   9 0 8
8 9 0   0 6 5   3 1 4
];
[C,L] = sudoku_candidates(X);
[X,C,L] = sudoku_findlocked ( X , C , L , %f );
//
// Found row locked candidate (type 1) 2 in block (4,7)
// Removed row locked candidate (type 1) 2 at (6,5) (remaining 73 candidates)
// Found row locked candidate (type 1) 4 in block (4,7)
// Removed row locked candidate (type 1) 4 at (6,1) (remaining 72 candidates)
// Found column locked candidate (type 1) 2 in block (7,4)
// Removed column locked candidate (type 1) 2 at (4,4) (remaining 71 candidates)
// Found column locked candidate (type 2) 5 in column 9
// Remove column locked candidate (type 2) 5 at (6,7) (remaining 70 candidates)
// Remove column locked candidate (type 2) 5 at (6,8) (remaining 69 candidates)
//
// Check that 2 is eliminated as candidate in (6,5)
// As a consequence, the 3 in (6,5) is the only candidate in its cell.
// Check that 4 is eliminated as candidate in (6,1)
// Check that 2 is eliminated as candidate in (4,4)
// Check that 5 is eliminated as candidate in (6,7) and (6,8)
assert_checkequal ( candcell ( C , 6,5) , 3 );
assert_checkequal ( candcell ( C , 6,1) , [3 5 7 9] );
assert_checkequal ( candcell ( C , 4,4) , [4 5] );
assert_checkequal ( candcell ( C , 6,7) , [2 4] );
assert_checkequal ( candcell ( C , 6,8) , [2 4] );
LE = [
0 2 0   0 0 0   2 0 0 
3 3 0   0 0 0   0 2 0 
3 0 2   0 0 0   0 2 0 
..
4 3 0   2 2 0   0 0 2 
2 0 0   2 0 0   0 0 0 
4 0 3   0 1 0   2 2 2 
..
2 0 0   0 0 0   2 0 0 
0 3 4   2 0 0   0 2 0 
0 0 2   2 0 0   0 0 0 
];
assert_checkequal ( L , LE );

////////////////////////////////////////////////////////////////
//
// A locked candidate type 2
// There are 2 candidates for digit 2 in column 7. 
// Both those candidates are locked in box 9. 
// Since one of these 2 cells must contain digit 2, no other cell in box 9 can contain this digit. 
// This eliminates the candidate 2 in cells (7,8) (7,9) (8,8) (8,9) (9,8) (9,9)
X = sudoku_readgivens("500200010001900730000000800050020008062039000000004300000000000080467900007300000");
[C,L] = sudoku_candidates(X);
[X,C,L] = sudoku_findlocked ( X , C , L , %f );
// the part of the grid which was under pressure : C([7 8 9],[7 8 9])
assert_checkequal ( candcell ( C , 7,8) , [4 5 6 7 8] );
assert_checkequal ( candcell ( C , 7,9) , [1 3 4 5 6 7] );
assert_checkequal ( candcell ( C , 8,8) , 5 );
assert_checkequal ( candcell ( C , 8,9) , [1 3 5] );
assert_checkequal ( candcell ( C , 9,8) , [4 5 6 8] );
assert_checkequal ( candcell ( C , 9,9) , [1 4 5 6] );
LE = [
0 4 5   0 3 3   2 0 3 
4 2 0   0 3 3   0 0 4 
6 5 4   4 4 4   0 5 5 
..
5 0 3   3 0 2   3 4 0 
4 0 0   4 0 0   3 3 4 
4 3 2   5 4 0   0 5 6 
..
6 5 5   3 4 4   5 5 6 
3 0 2   0 0 0   0 1 3 
5 4 0   0 4 4   5 4 4 
];
assert_checkequal ( L , LE );
////////////////////////////////////////
// Test stopatfirst

X = [
1 0 8   6 7 2   0 9 3
0 0 9   8 1 4   6 0 2
0 6 0   9 5 3   8 0 1
..
0 0 6   0 0 7   1 8 0
0 2 1   0 9 8   7 3 6
0 8 0   1 0 6   0 0 0
..
0 1 4   3 8 9   0 6 7
6 0 0   0 4 1   9 0 8
8 9 0   0 6 5   3 1 4
];
[C,L] = sudoku_candidates(X);
// Found row locked candidate (type 1) 2 in block (4,7)
// Removed row locked candidate (type 1) 2 at (6,5) (remaining 73 candidates)
[X,C,L] = sudoku_findlocked ( X , C , L , %t );
assert_checkequal ( sum(L) , 73 );
// Found row locked candidate (type 1) 4 in block (4,7)
// Removed row locked candidate (type 1) 4 at (6,1) (remaining 72 candidates)
[X,C,L] = sudoku_findlocked ( X , C , L , %t );
assert_checkequal ( sum(L) , 72 );
// Found column locked candidate (type 1) 2 in block (7,4)
// Removed column locked candidate (type 1) 2 at (4,4) (remaining 71 candidates)
[X,C,L] = sudoku_findlocked ( X , C , L , %t );
assert_checkequal ( sum(L) , 71 );
// Found column locked candidate (type 2) 5 in column 9
// Remove column locked candidate (type 2) 5 at (6,7) (remaining 70 candidates)
// Remove column locked candidate (type 2) 5 at (6,8) (remaining 69 candidates)
[X,C,L] = sudoku_findlocked ( X , C , L , %t );
assert_checkequal ( sum(L) , 69 );

