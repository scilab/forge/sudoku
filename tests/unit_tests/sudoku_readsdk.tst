// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->
// <-- ENGLISH IMPOSED -->

path = sudoku_getpath();
filename = fullfile(path,"tests","unit_tests","minimal.sdk");

X = sudoku_readsdk ( filename );
XE = [
6 0 0   0 0 0   0 0 7 
0 0 0   0 9 0   0 2 0 
3 0 1   0 0 2   5 9 0 
..
8 0 0   0 0 7   0 1 3 
0 0 0   0 8 0   0 0 0 
7 6 0   3 0 0   0 0 8 
..
0 7 8   2 0 0   1 0 6 
0 5 0   0 3 0   0 0 0 
2 0 0   0 0 0   0 0 9 
];
assert_checkequal ( X , XE );
//
//
//
filename = fullfile(path,"tests","unit_tests","minimal2.sdk");
X = sudoku_readsdk (filename);
XE = [
6 0 0   0 0 0   0 0 7 
0 0 0   0 9 0   0 2 0 
3 0 1   0 0 2   5 9 0 
..
8 0 0   0 0 7   0 1 3 
0 0 0   0 8 0   0 0 0 
7 6 0   3 0 0   0 0 8 
..
0 7 8   2 0 0   1 0 6 
0 5 0   0 3 0   0 0 0 
2 0 0   0 0 0   0 0 9 
];
assert_checkequal ( X , XE );

