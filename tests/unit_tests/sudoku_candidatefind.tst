// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->
// <-- ENGLISH IMPOSED -->



X = [
0 0 0   0 0 0   0 0 0
0 0 0   0 0 0   5 3 0
9 0 0   0 3 7   0 0 0
0 0 2   3 0 1   0 9 6
0 4 7   6 9 0   1 8 0
0 0 6   7 8 5   0 0 0
0 5 0   0 2 0   9 0 3
0 3 0   9 0 0   6 0 8
8 0 0   0 1 0   4 7 0
];
//
[C,L] = sudoku_candidates(X);
[rows,cols] = sudoku_candidatefind(X,C,L,2);
assert_checkequal ( rows' , [1 2 8 1 2 3 9 1 2 3 1 2 5 1 3 6 1 3 6 8 1 2 3 5 6 9] );
assert_checkequal ( cols' , [1 1 1 2 2 2 2 4 4 4 6 6 6 7 7 7 8 8 8 8 9 9 9 9 9 9] );

