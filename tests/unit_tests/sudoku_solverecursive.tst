// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->
// <-- ENGLISH IMPOSED -->




// Naked singles alone are sufficient
X = [
0 0 0   0 0 0   0 0 0
0 0 0   0 0 0   5 3 0
9 0 0   0 3 7   0 0 0
0 0 2   3 0 1   0 9 6
0 4 7   6 9 0   1 8 0
0 0 6   7 8 5   0 0 0
0 5 0   0 2 0   9 0 3
0 3 0   9 0 0   6 0 8
8 0 0   0 1 0   4 7 0
];
Y = sudoku_solverecursive ( X );
f = sudoku_issolved(Y);
assert_checkequal ( f , %t );
E = [
    6    7    3    1    5    8    2    4    9  
    4    1    8    2    6    9    5    3    7  
    9    2    5    4    3    7    8    6    1  
    5    8    2    3    4    1    7    9    6  
    3    4    7    6    9    2    1    8    5  
    1    9    6    7    8    5    3    2    4  
    7    5    4    8    2    6    9    1    3  
    2    3    1    9    7    4    6    5    8  
    8    6    9    5    1    3    4    7    2  
 ];
assert_checkequal ( Y , E );
////////////////////////////////////////////////////////
// Guessing is now necessary:
// beware, the solution is not unique anymore !
X = [
0 0 0   0 0 0   0 0 0
0 0 0   0 0 0   0 3 0
9 0 0   0 3 7   0 0 0
0 0 2   3 0 1   0 9 6
0 4 7   6 9 0   1 8 0
0 0 6   7 8 5   0 0 0
0 5 0   0 2 0   9 0 3
0 3 0   9 0 0   6 0 8
8 0 0   0 1 0   4 7 0
];
[Y,ml] = sudoku_solverecursive ( X );
f = sudoku_issolved(Y);
assert_checkequal ( f , %t );
assert_checkequal ( ml>0 , %t );

