// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->
// <-- ENGLISH IMPOSED -->



X = sudoku_readgivens ("6....23..1256.......47...2.73....84...........46....15.5...81.......3472..72....8");
XE = [
6 0 0   0 0 2   3 0 0 
1 2 5   6 0 0   0 0 0 
0 0 4   7 0 0   0 2 0 
..
7 3 0   0 0 0   8 4 0 
0 0 0   0 0 0   0 0 0 
0 4 6   0 0 0   0 1 5 
..
0 5 0   0 0 8   1 0 0 
0 0 0   0 0 3   4 7 2 
0 0 7   2 0 0   0 0 8 
];
assert_checkequal ( X , XE );


