// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->
// <-- ENGLISH IMPOSED -->


path = sudoku_getpath();
filename = fullfile(path,"tests","unit_tests","mycollection.sdm");
X = sudoku_readsdm (filename,3);
str = sudoku_writess ( X );
E =[
"9..|7.3|..4|"
"6..|.9.|2..|"
"..7|.6.|...|"
"-----------"
".8.|...|5..|"
"..6|.4.|7..|"
"..9|...|.8.|"
"-----------"
"...|.5.|8..|"
"..1|.2.|..3|"
"2..|9.1|..5|"
];
assert_checkequal(str , E );
//
filename = TMPDIR+"\writess.ss";
str = sudoku_writess ( X , filename );
assert_checkequal ( fileinfo(filename)<>[] , %t );


