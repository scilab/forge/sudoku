// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->
// <-- ENGLISH IMPOSED -->


////////////////////////////////////////
// An easy sudoku with naked singles only.
X = [
0 0 0   0 0 0   0 0 0 
0 0 0   0 0 0   5 3 0 
9 0 0   0 3 7   0 0 0 
..
0 0 2   3 0 1   0 9 6 
0 4 7   6 9 0   1 8 0 
0 0 6   7 8 5   0 0 0 
..
0 5 0   0 2 0   9 0 3 
0 3 0   9 0 0   6 0 8 
8 0 0   0 1 0   4 7 0 
];
[Y,niter,C,L,conflict] = sudoku_solvebylogic(X);
solved = sudoku_issolved(Y);
assert_checkequal ( solved , %t );
assert_checkequal ( niter(1) , 3 );
assert_checkequal ( conflict , %f );
assert_checkequal ( and(L==0) , %t );

////////////////////////////////////////
// An easy sudoku with naked singles and hidden singles.

X = [
0 0 0   0 0 0   0 0 0 
9 0 0   0 0 4   0 3 6 
0 0 0   0 7 2   0 0 9 
..
3 0 0   0 0 0   0 0 5 
0 1 4   0 0 0   8 0 0 
0 2 5   0 1 0   0 0 0 
..
0 0 6   1 0 0   0 0 3 
0 0 0   0 0 6   0 4 0 
0 0 2   4 8 9   0 0 0 
];
[Y,niter,C,L,conflict] = sudoku_solvebylogic(X);
solved = sudoku_issolved(Y);
assert_checkequal ( solved , %t );
assert_checkequal ( niter(1) , 6 );
assert_checkequal ( niter(2) , 1 );
assert_checkequal ( conflict , %f );
assert_checkequal ( and(L==0) , %t );

////////////////////////////////////////
// An impossible sudoku
// Naked singles are required, then a conflict is detected.
X = [
1 2 0   0 3 0   0 4 0 
6 0 0   0 0 0   0 0 3 
3 0 4   0 0 0   5 0 0 
..
2 0 0   8 0 6   0 0 0 
8 0 0   0 1 0   0 0 6 
0 0 0   7 0 5   0 0 0 
..
0 0 7   0 0 0   6 0 0 
4 0 0   0 0 0   0 0 8 
0 3 0   0 4 0   0 2 0 
];
[Y,niter,C,L,conflict] = sudoku_solvebylogic(X);
solved = sudoku_issolved(Y);
assert_checkequal ( solved , %f );
assert_checkequal ( conflict , %t );

// Cannot assert_checkequal ( niter , [13 2 1 1 1 1] ) : depends on the run
////////////////////////////////////////
// A difficult sudoku !
if ( %f ) then
X = [
1 2 0   0 3 0   0 4 0 
0 0 0   0 0 0   0 0 0 
0 0 0   0 0 0   0 0 0 
..
0 0 0   0 0 0   0 0 0 
0 0 0   0 0 0   0 0 0 
0 0 0   0 0 0   0 0 0 
..
0 0 0   0 0 0   0 0 0 
0 0 0   0 0 0   0 0 0 
0 0 0   0 0 0   0 0 0 
];
[Y,niter] = sudoku_solvebylogic(X);
solved = sudoku_issolved(Y);
assert_checkequal ( solved , %t );
end

////////////////////////////////////////
// An inconsistent sudoku

X = [
1 2 0   0 3 0   0 4 0 
3 5 0   0 0 0   0 0 0 
4 6 0   0 0 0   0 0 0 
..
2 1 0   0 0 0   0 0 0 
5 3 0   0 0 0   0 0 0 
6 0 0   0 0 0   0 0 0 
..
7 0 0   0 0 0   0 0 0 
8 0 0   0 0 0   0 0 0 
9 0 0   0 0 0   0 0 0 
];
[Y,niter,C,L,conflict] = sudoku_solvebylogic(X);
solved = sudoku_issolved(Y);
assert_checkequal ( solved , %f );
assert_checkequal ( conflict , %t );

////////////////////////////////////////
// A sudoku a little more difficult than average
// Allows to check that the condition for a successful
// pass is that the number of unknown OR the number of 
// candidates has been updated.
X = [
8 3 0   0 0 9   0 2 6 
0 0 9   0 0 0   0 0 0 
2 0 0   0 0 0   8 0 1 
..
0 0 2   0 0 7   0 0 0 
0 0 0   0 0 2   4 6 0 
0 4 7   6 0 0   0 1 0 
..
7 0 0   0 0 8   0 0 9 
0 0 0   0 0 0   0 0 0 
0 0 6   0 3 5   7 0 0 
];
[Y,niter,C,L,conflict] = sudoku_solvebylogic(X);
solved = sudoku_issolved(Y);
assert_checkequal ( solved , %t );
assert_checkequal ( conflict , %f );
assert_checkequal ( and(L==0) , %t );

////////////////////////////////////////
// Detect conflicts in sudokus
// There is no candidate for cell (2,6).
X = [
6 7 3   1 5 9   2 4 9 
4 1 8   2 6 0   5 3 7 
9 2 5   4 3 7   8 6 1 
..
5 8 2   3 4 1   7 9 6 
3 4 7   6 9 2   1 8 5 
1 9 6   7 8 5   3 2 4 
..
7 5 4   8 2 6   9 1 3 
2 3 1   9 7 4   6 5 8 
8 6 9   5 1 3   4 7 2 
];
[Y,niter,C,L,conflict] = sudoku_solvebylogic(X);
solved = sudoku_issolved(Y);
assert_checkequal ( solved , %f );
assert_checkequal ( conflict , %t );


