// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->
// <-- ENGLISH IMPOSED -->
// <-- NO CHECK REF -->



// tri --
//   tri(1) = [1 2 3], tri(2) = [1 2 3], tri(3) = [1 2 3]
//   tri(4) = [4 5 6], tri(5) = [4 5 6], tri(6) = [4 5 6]
//   tri(7) = [7 8 9], tri(8) = [7 8 9], tri(9) = [7 8 9]
function y = tri ( k )
  y = 3*ceil(k/3-1) + (1:3)
endfunction

////////////////////////////////////////////////////////////////////
// Solve a grid with simulated annealing
// Make sure that the loop in the neighbourhood function does not fail
// when searching a block with two cells to swap.
X = [
2 6 0   3 9 0   1 5 4
7 4 3   1 5 0   0 9 0 
1 9 0   4 2 6   3 0 0 
..
8 0 0   5 0 9   4 1 0 
9 7 4   8 6 1   2 3 5 
0 5 1   0 4 3   7 8 9 
..
4 1 9   6 3 5   0 2 7 
3 2 0   9 0 4   5 0 1 
5 8 6   7 1 2   9 4 3 
];
grand("setsd",123456);
[Y,iter] = sudoku_solvesa ( X , 100 , %t );
solved = sudoku_issolved(Y);
assert_checkequal ( solved , %t );
assert_checkequal ( iter>0 , %t );


