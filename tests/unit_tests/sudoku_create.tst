// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->
// <-- ENGLISH IMPOSED -->


if ( %f ) then
////////////////////////////////////////
// Create a sudoku from nothing
//
[puzzle,solution] = sudoku_create(%f,28);
assert_checkequal ( sudoku_issolved(solution) , %t );
assert_checkequal ( sudoku_issolved(puzzle) , %f );
assert_checkequal ( find(puzzle>0) == find(solution>0) , %f );
S = sudoku_solve(puzzle);
assert_checkequal ( S(1) , solution );

////////////////////////////////////////
// Create a sudoku from a solution
//
// Compute a target number of givens:
// a Gauss-Laplace curve around 28, never more than 32
clues = round(grand(1,1,"nor",28,1))
clues = min ( clues , 32 )
solution = sudoku_readgivens("271649853634815972598273614862957431947381265315426798789532146456198327123764589");
[puzzle,solution] = sudoku_create(%f,clues,100,solution);
assert_checkequal ( sudoku_issolved(solution) , %t );
assert_checkequal ( sudoku_issolved(puzzle) , %f );
assert_checkequal ( find(puzzle>0) == find(solution>0) , %f );
S = sudoku_solve(puzzle);
assert_checkequal ( S(1) , solution );

end

